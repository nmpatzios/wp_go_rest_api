package mysqldb

import (
	"database/sql"
	"fmt"
	"log"
	"models"

	"crypto/x509"

	"crypto/tls"
	"io/ioutil"

	"github.com/go-sql-driver/mysql"
	logurus "github.com/sirupsen/logrus"
)

func Connect(c models.Conf) *sql.DB {
	rootCertPool := x509.NewCertPool()
	pem, _ := ioutil.ReadFile("BaltimoreCyberTrustRoot.crt.pem")
	if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
		log.Fatal("Failed to append PEM.")
	}
	mysql.RegisterTLSConfig("custom", &tls.Config{RootCAs: rootCertPool, InsecureSkipVerify: true})
	var connectionString string
	connectionString = fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?allowNativePasswords=true&tls=custom&parseTime=true", c.MYSQLUSER, models.DecryptPass(models.KeyGenerator, c.MYSQLPASS), "nikosmysqldatatabsetest.mysql.database.azure.com", c.MYSQLDB)
	db, err := sql.Open("mysql", connectionString)

	if err != nil {
		logurus.Fatal("Could not connect to database")
	}

	err = db.Ping()
	if err != nil {
		logurus.Fatal("Could not connect to database" + err.Error())
	}

	return db
}
