package boltdb

import (
	"encoding/json"
	"fmt"

	"github.com/boltdb/bolt"
	logurus "github.com/sirupsen/logrus"
)

func SetCustomerKeySecret(db *bolt.DB, config CustomerKeysecret) error {
	confBytes, err := json.Marshal(config)
	if err != nil {
		return fmt.Errorf("could not marshal config json: %v", err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		err = tx.Bucket([]byte("DB")).Put([]byte("CUSTOMERKEYSECRET"), confBytes)
		if err != nil {
			return fmt.Errorf("could not set config: %v", err)
		}
		return nil
	})
	return err
}

func GetCusotmerKeySecret(db *bolt.DB, config *CustomerKeysecret) (*CustomerKeysecret, error) {
	err := db.View(func(tx *bolt.Tx) error {
		conf := tx.Bucket([]byte("DB")).Get([]byte("CUSTOMERKEYSECRET"))
		// fmt.Printf("Config: %s\n", conf)

		err := json.Unmarshal(conf, &config)
		if err != nil {
			logurus.Error(err.Error())
			return err
		}
		return nil
	})
	if err != nil {
		logurus.Error(err.Error())
		return &CustomerKeysecret{}, err
	}

	return config, nil
}
