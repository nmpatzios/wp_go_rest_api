package woocommerce

import "strings"

type Order struct {
	ID                 int            `json:"id,omitempty"`
	ParentID           int            `json:"parent_id,omitempty"`
	Number             string         `json:"number,omitempty"`
	OrderKey           string         `json:"order_key,omitempty"`
	CreatedVia         string         `json:"created_via,omitempty"`
	Versions           string         `json:"version,omitempty"`
	Status             string         `json:"status,omitempty"`
	Currency           string         `json:"currency,omitempty"`
	DateCreated        string         `json:"date_created,omitempty"`
	DateCreatedGMT     string         `json:"date_created_gmt,omitempty"`
	DateModified       string         `json:"date_modified,omitempty"`
	DateModifiedGMT    string         `json:"date_modified_gmt,omitempty"`
	DiscountTotal      string         `json:"discount_total,omitempty"`
	DiscountTax        string         `json:"discount_tax,omitempty"`
	ShippingTotal      string         `json:"shipping_total,omitempty"`
	ShippingTax        string         `json:"shipping_tax,omitempty"`
	CartTax            string         `json:"cart_tax,omitempty"`
	Total              string         `json:"total,omitempty"`
	TotalTax           string         `json:"total_tax,omitempty"`
	PricesIncludeTax   bool           `json:"prices_include_tax,omitempty"`
	CustomnerID        int            `json:"customer_id,omitempty"`
	CustomerIPAddress  string         `json:"customer_ip_address,omitempty"`
	CustomerUserAgent  string         `json:"customer_user_agent,omitempty"`
	CustomerNote       string         `json:"customer_note,omitempty"`
	Billing            Billing        `json:"billing,omitempty"`
	Shipping           Shipping       `json:"shipping,omitempty"`
	PaymentMethod      string         `json:"payment_method,omitempty"`
	PaymentMethodTitle string         `json:"payment_method_title,omitempty"`
	TransactionID      string         `json:"transaction_id,omitempty"`
	DatePaid           string         `json:"date_paid,omitempty"`
	DatePaidGMT        string         `json:"date_paid_gmt,omitempty"`
	DateCompleted      string         `json:"date_completed,omitempty"`
	DateCompletedGMT   string         `json:"date_completed_gmt,omitempty"`
	CardHash           string         `json:"cart_hash,omitempty"`
	MetaData           []LanLot       `json:"meta_data,omitempty"`
	LineItems          []LineItem     `json:"line_items,omitempty"`
	TaxLines           interface{}    `json:"tax_lines,omitempty"`
	ShippingLines      []ShippingLine `json:"shipping_lines,omitempty"`
	FeeLines           interface{}    `json:"fee_lines,omitempty"`
	CouponLines        interface{}    `json:"coupon_lines,omitempty"`
	Refunds            interface{}    `json:"refunds,omitempty"`
	Links              Links          `json:"_links,omitempty"`
	ShippingMethod     string         `json:"-"`
	Lat                interface{}    `json:"-"`
	Lon                interface{}    `json:"-"`
}

type LanLot struct {
	ID    int         `json:"id,omitempty"`
	Key   string      `json:"key,omitempty"`
	Value interface{} `json:"value,omitempty"`
}

type CouponLines struct {
	ID          int    `json:"id"`
	Code        string `json:"code"`
	Discount    string `json:"discount"`
	DiscountTax string `json:"discount_tax"`
	MetaData    []struct {
		ID    int    `json:"id"`
		Key   string `json:"key"`
		Value struct {
			ID          int    `json:"id"`
			Code        string `json:"code"`
			Amount      string `json:"amount"`
			DateCreated struct {
				Date         string `json:"date"`
				TimezoneType int    `json:"timezone_type"`
				Timezone     string `json:"timezone"`
			} `json:"date_created"`
			DateModified struct {
				Date         string `json:"date"`
				TimezoneType int    `json:"timezone_type"`
				Timezone     string `json:"timezone"`
			} `json:"date_modified"`
			DateExpires               interface{}   `json:"date_expires"`
			DiscountType              string        `json:"discount_type"`
			Description               string        `json:"description"`
			UsageCount                int           `json:"usage_count"`
			IndividualUse             bool          `json:"individual_use"`
			ProductIds                []interface{} `json:"product_ids"`
			ExcludedProductIds        []interface{} `json:"excluded_product_ids"`
			UsageLimit                int           `json:"usage_limit"`
			UsageLimitPerUser         int           `json:"usage_limit_per_user"`
			LimitUsageToXItems        interface{}   `json:"limit_usage_to_x_items"`
			FreeShipping              bool          `json:"free_shipping"`
			ProductCategories         []interface{} `json:"product_categories"`
			ExcludedProductCategories []interface{} `json:"excluded_product_categories"`
			ExcludeSaleItems          bool          `json:"exclude_sale_items"`
			MinimumAmount             string        `json:"minimum_amount"`
			MaximumAmount             string        `json:"maximum_amount"`
			EmailRestrictions         []interface{} `json:"email_restrictions"`
			Virtual                   bool          `json:"virtual"`
			MetaData                  []struct {
				ID    int    `json:"id"`
				Key   string `json:"key"`
				Value string `json:"value"`
			} `json:"meta_data"`
		} `json:"value"`
	} `json:"meta_data"`
}

type Billing struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	City      string `json:"city"`
	State     string `json:"state"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
}
type Shipping struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	City      string `json:"city"`
	State     string `json:"state"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
}
type Links struct {
	Self       []Self       `json:"self"`
	Collection []Collection `json:"collection"`
	Customer   []Customer   `json:"customer"`
}
type Self struct {
	Href string `json:"href"`
}
type Collection struct {
	Href string `json:"href"`
}
type Customer struct {
	Href string `json:"href"`
}

type LineItem struct {
	ID          int                `json:"id"`
	Name        string             `json:"name"`
	ProductID   int                `json:"product_id"`
	VariationID int                `json:"variation_id"`
	Quantity    int                `json:"quantity"`
	TaxClass    string             `json:"tax_class"`
	Subtotal    string             `json:"subtotal"`
	SubTotalTax string             `json:"subtotal_tax"`
	Total       string             `json:"total"`
	TotalTax    string             `json:"total_tax"`
	Taxes       interface{}        `json:"taxes"`
	MetaData    []LineItemMetaData `json:"meta_data"`
	SKU         string             `json:"sku"`
	Price       float64            `json:"price"`
}

type ShippingLine struct {
	ID          int                    `json:"id"`
	MethodTitle string                 `json:"method_title"`
	MethodID    string                 `json:"method_id"`
	InstanceID  string                 `json:"instance_id"`
	Total       string                 `json:"total"`
	TotalTax    string                 `json:"total_tax"`
	Taxes       interface{}            `json:"taxes"`
	MetaData    []ShippingLineMetaData `json:"meta_data"`
}
type ShippingLineMetaData struct {
	ID    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"value"`
}

type LineItemMetaData struct {
	ID    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"value"`
}

type WPCustomer struct {
	ID               int             `json:"id"`
	DateCreated      string          `json:"date_created"`
	DateCreatedGmt   string          `json:"date_created_gmt"`
	DateModified     string          `json:"date_modified"`
	DateModifiedGmt  string          `json:"date_modified_gmt"`
	Email            string          `json:"email"`
	FirstName        string          `json:"first_name"`
	LastName         string          `json:"last_name"`
	Role             string          `json:"role"`
	Username         string          `json:"username"`
	IsPayingCustomer bool            `json:"is_paying_customer"`
	OrdersCount      int             `json:"orders_count"`
	TotalSpent       string          `json:"total_spent"`
	AvatarURL        string          `json:"avatar_url"`
	BillingAddress   BillingAddress  `json:"billing"`
	ShippingAddress  ShippingAddress `json:"shipping"`
	MetaData         []struct {
		ID    int         `json:"id"`
		Key   string      `json:"key"`
		Value interface{} `json:"value"`
	} `json:"meta_data"`
	Links struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
	} `json:"_links"`
}

type BillingAddress struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	City      string `json:"city"`
	State     string `json:"state"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
}

type ShippingAddress struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	City      string `json:"city"`
	State     string `json:"state"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
}

type Products struct {
	Products []Product
}

type Product struct {
	ID                int           `json:"id"`
	Name              string        `json:"name"`
	Slug              string        `json:"slug"`
	Permalink         string        `json:"permalink"`
	DateCreated       string        `json:"date_created"`
	DateCreatedGmt    string        `json:"date_created_gmt"`
	DateModified      string        `json:"date_modified"`
	DateModifiedGmt   string        `json:"date_modified_gmt"`
	Type              string        `json:"type"`
	Status            string        `json:"status"`
	Featured          bool          `json:"featured"`
	CatalogVisibility string        `json:"catalog_visibility"`
	Description       string        `json:"description"`
	PackageFeatures   []string      `json:"-"`
	ShortDescription  string        `json:"short_description"`
	Sku               string        `json:"sku"`
	Price             string        `json:"price"`
	RegularPrice      string        `json:"regular_price"`
	SalePrice         string        `json:"sale_price"`
	DateOnSaleFrom    interface{}   `json:"date_on_sale_from"`
	DateOnSaleFromGmt interface{}   `json:"date_on_sale_from_gmt"`
	DateOnSaleTo      interface{}   `json:"date_on_sale_to"`
	DateOnSaleToGmt   interface{}   `json:"date_on_sale_to_gmt"`
	PriceHTML         string        `json:"price_html"`
	OnSale            bool          `json:"on_sale"`
	Purchasable       bool          `json:"purchasable"`
	TotalSales        int           `json:"total_sales"`
	Virtual           bool          `json:"virtual"`
	Downloadable      bool          `json:"downloadable"`
	Downloads         []interface{} `json:"downloads"`
	DownloadLimit     int           `json:"download_limit"`
	DownloadExpiry    int           `json:"download_expiry"`
	ExternalURL       string        `json:"external_url"`
	ButtonText        string        `json:"button_text"`
	TaxStatus         string        `json:"tax_status"`
	TaxClass          string        `json:"tax_class"`
	ManageStock       bool          `json:"manage_stock"`
	StockQuantity     interface{}   `json:"stock_quantity"`
	InStock           bool          `json:"in_stock"`
	Backorders        string        `json:"backorders"`
	BackordersAllowed bool          `json:"backorders_allowed"`
	Backordered       bool          `json:"backordered"`
	SoldIndividually  bool          `json:"sold_individually"`
	Weight            string        `json:"weight"`
	Dimensions        Dimensions    `json:"dimensions"`
	ShippingRequired  bool          `json:"shipping_required"`
	ShippingTaxable   bool          `json:"shipping_taxable"`
	ShippingClass     string        `json:"shipping_class"`
	ShippingClassID   int           `json:"shipping_class_id"`
	ReviewsAllowed    bool          `json:"reviews_allowed"`
	AverageRating     string        `json:"average_rating"`
	RatingCount       int           `json:"rating_count"`
	RelatedIds        []int         `json:"related_ids"`
	UpsellIds         []interface{} `json:"upsell_ids"`
	CrossSellIds      []interface{} `json:"cross_sell_ids"`
	ParentID          int           `json:"parent_id"`
	PurchaseNote      string        `json:"purchase_note"`
	Categories        []Categories  `json:"categories"`
	Tags              []interface{} `json:"tags"`
	Images            []Images      `json:"images"`
	Attributes        []interface{} `json:"attributes"`
	DefaultAttributes []interface{} `json:"default_attributes"`
	Variations        []interface{} `json:"variations"`
	GroupedProducts   []interface{} `json:"grouped_products"`
	MenuOrder         int           `json:"menu_order"`
	MetaData          []MetaData    `json:"meta_data"`
	Links             ProductLinks  `json:"_links"`
}

func (p *Product) ProductName() string {
	return strings.Join(strings.Split(p.Name, " "), "_")
}

type ProductLinks struct {
	Self []struct {
		Href string `json:"href"`
	} `json:"self"`
	Collection []struct {
		Href string `json:"href"`
	} `json:"collection"`
}
type MetaData struct {
	ID    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"-"`
}
type Images struct {
	ID              int    `json:"id"`
	DateCreated     string `json:"date_created"`
	DateCreatedGmt  string `json:"date_created_gmt"`
	DateModified    string `json:"date_modified"`
	DateModifiedGmt string `json:"date_modified_gmt"`
	Src             string `json:"src"`
	Name            string `json:"name"`
	Alt             string `json:"alt"`
	Position        int    `json:"position"`
}
type Categories struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Slug string `json:"slug"`
}
type Dimensions struct {
	Length string `json:"length"`
	Width  string `json:"width"`
	Height string `json:"height"`
}

type ProductCategories struct {
	Categories []ProductCategory
}

type ProductCategory struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Slug        string `json:"slug"`
	Parent      int    `json:"parent"`
	Description string `json:"description"`
	Display     string `json:"display"`
	Image       string `json:"image"`
	Count       int    `json:"count"`

	Checked bool `json:"-"`
}
type OrderNote struct {
	ID             int    `json:"id"`
	Author         string `json:"author"`
	DateCreated    string `json:"date_created"`
	DateCreatedGmt string `json:"date_created_gmt"`
	Note           string `json:"note"`
	CustomerNote   bool   `json:"customer_note"`
	Links          struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
		Up []struct {
			Href string `json:"href"`
		} `json:"up"`
	} `json:"_links"`
}

type ProductsStatus struct {
	ID     int    `json:"id"`
	Status string `json:"status"`
}

type T int

type Total struct {
	Slug  string `json:"slug"`
	Name  string `json:"name"`
	Total int    `json:"total"`
}

type TotalsAmount struct {
	Total       []Total `json:"totals"`
	TotalAmount string  `json:"total_amount"`
	TotalToday  string  `json:"total_today"`
}

type TotalAMounts struct {
	TotalSales      string                 `json:"total_sales"`
	NetSales        string                 `json:"net_sales"`
	AverageSales    string                 `json:"average_sales"`
	TotalOrders     int                    `json:"total_orders"`
	TotalItems      int                    `json:"total_items"`
	TotalTax        string                 `json:"total_tax"`
	TotalShipping   string                 `json:"total_shipping"`
	TotalRefunds    int                    `json:"total_refunds"`
	TotalDiscount   string                 `json:"total_discount"`
	TotalsGroupedBy string                 `json:"totals_grouped_by"`
	Totals          map[string]interface{} `json:"totals"`
	TotalCustomers  int                    `json:"total_customers"`
	Links           struct {
		About []struct {
			Href string `json:"href"`
		} `json:"about"`
	} `json:"_links"`
}

type Completedorders struct {
	ID                int    `json:"id"`
	ParentID          int    `json:"parent_id"`
	Number            string `json:"number"`
	OrderKey          string `json:"order_key"`
	CreatedVia        string `json:"created_via"`
	Version           string `json:"version"`
	Status            string `json:"status"`
	Currency          string `json:"currency"`
	DateCreated       string `json:"date_created"`
	DateCreatedGmt    string `json:"date_created_gmt"`
	DateModified      string `json:"date_modified"`
	DateModifiedGmt   string `json:"date_modified_gmt"`
	DiscountTotal     string `json:"discount_total"`
	DiscountTax       string `json:"discount_tax"`
	ShippingTotal     string `json:"shipping_total"`
	ShippingTax       string `json:"shipping_tax"`
	CartTax           string `json:"cart_tax"`
	Total             string `json:"total"`
	TotalTax          string `json:"total_tax"`
	PricesIncludeTax  bool   `json:"prices_include_tax"`
	CustomerID        int    `json:"customer_id"`
	CustomerIPAddress string `json:"customer_ip_address"`
	CustomerUserAgent string `json:"customer_user_agent"`
	CustomerNote      string `json:"customer_note"`
	Billing           struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Company   string `json:"company"`
		Address1  string `json:"address_1"`
		Address2  string `json:"address_2"`
		City      string `json:"city"`
		State     string `json:"state"`
		Postcode  string `json:"postcode"`
		Country   string `json:"country"`
		Email     string `json:"email"`
		Phone     string `json:"phone"`
	} `json:"billing"`
	Shipping struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Company   string `json:"company"`
		Address1  string `json:"address_1"`
		Address2  string `json:"address_2"`
		City      string `json:"city"`
		State     string `json:"state"`
		Postcode  string `json:"postcode"`
		Country   string `json:"country"`
	} `json:"shipping"`
	PaymentMethod      string        `json:"payment_method"`
	PaymentMethodTitle string        `json:"payment_method_title"`
	TransactionID      string        `json:"transaction_id"`
	DatePaid           string        `json:"date_paid"`
	DatePaidGmt        string        `json:"date_paid_gmt"`
	DateCompleted      string        `json:"date_completed"`
	DateCompletedGmt   string        `json:"date_completed_gmt"`
	CartHash           string        `json:"cart_hash"`
	MetaData           []interface{} `json:"meta_data"`
	LineItems          []struct {
		ID          int           `json:"id"`
		Name        string        `json:"name"`
		ProductID   int           `json:"product_id"`
		VariationID int           `json:"variation_id"`
		Quantity    int           `json:"quantity"`
		TaxClass    string        `json:"tax_class"`
		Subtotal    string        `json:"subtotal"`
		SubtotalTax string        `json:"subtotal_tax"`
		Total       string        `json:"total"`
		TotalTax    string        `json:"total_tax"`
		Taxes       []interface{} `json:"taxes"`
		MetaData    []struct {
			ID    int    `json:"id"`
			Key   string `json:"key"`
			Value string `json:"value"`
		} `json:"meta_data"`
		Sku   string  `json:"sku"`
		Price float64 `json:"price"`
	} `json:"line_items"`
	TaxLines      []interface{} `json:"tax_lines"`
	ShippingLines []interface{} `json:"shipping_lines"`
	FeeLines      []interface{} `json:"fee_lines"`
	CouponLines   []interface{} `json:"coupon_lines"`
	Refunds       []interface{} `json:"refunds"`
	Links         struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
		Customer []struct {
			Href string `json:"href"`
		} `json:"customer"`
	} `json:"_links"`
}

type OrdersPerCustomer struct {
	ID                int    `json:"id"`
	ParentID          int    `json:"parent_id"`
	Number            string `json:"number"`
	OrderKey          string `json:"order_key"`
	CreatedVia        string `json:"created_via"`
	Version           string `json:"version"`
	Status            string `json:"status"`
	Currency          string `json:"currency"`
	DateCreated       string `json:"date_created"`
	DateCreatedGmt    string `json:"date_created_gmt"`
	DateModified      string `json:"date_modified"`
	DateModifiedGmt   string `json:"date_modified_gmt"`
	DiscountTotal     string `json:"discount_total"`
	DiscountTax       string `json:"discount_tax"`
	ShippingTotal     string `json:"shipping_total"`
	ShippingTax       string `json:"shipping_tax"`
	CartTax           string `json:"cart_tax"`
	Total             string `json:"total"`
	TotalTax          string `json:"total_tax"`
	PricesIncludeTax  bool   `json:"prices_include_tax"`
	CustomerID        int    `json:"customer_id"`
	CustomerIPAddress string `json:"customer_ip_address"`
	CustomerUserAgent string `json:"customer_user_agent"`
	CustomerNote      string `json:"customer_note"`
	Billing           struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Company   string `json:"company"`
		Address1  string `json:"address_1"`
		Address2  string `json:"address_2"`
		City      string `json:"city"`
		State     string `json:"state"`
		Postcode  string `json:"postcode"`
		Country   string `json:"country"`
		Email     string `json:"email"`
		Phone     string `json:"phone"`
	} `json:"billing"`
	Shipping struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Company   string `json:"company"`
		Address1  string `json:"address_1"`
		Address2  string `json:"address_2"`
		City      string `json:"city"`
		State     string `json:"state"`
		Postcode  string `json:"postcode"`
		Country   string `json:"country"`
	} `json:"shipping"`
	PaymentMethod      string `json:"payment_method"`
	PaymentMethodTitle string `json:"payment_method_title"`
	TransactionID      string `json:"transaction_id"`
	DatePaid           string `json:"date_paid"`
	DatePaidGmt        string `json:"date_paid_gmt"`
	DateCompleted      string `json:"date_completed"`
	DateCompletedGmt   string `json:"date_completed_gmt"`
	CartHash           string `json:"cart_hash"`
	MetaData           []struct {
		ID    int    `json:"id"`
		Key   string `json:"key"`
		Value string `json:"value"`
	} `json:"meta_data"`
	LineItems []struct {
		ID          int           `json:"id"`
		Name        string        `json:"name"`
		ProductID   int           `json:"product_id"`
		VariationID int           `json:"variation_id"`
		Quantity    int           `json:"quantity"`
		TaxClass    string        `json:"tax_class"`
		Subtotal    string        `json:"subtotal"`
		SubtotalTax string        `json:"subtotal_tax"`
		Total       string        `json:"total"`
		TotalTax    string        `json:"total_tax"`
		Taxes       []interface{} `json:"taxes"`
		MetaData    []struct {
			ID    int    `json:"id"`
			Key   string `json:"key"`
			Value string `json:"value"`
		} `json:"meta_data"`
		Sku   string  `json:"sku"`
		Price float64 `json:"price"`
	} `json:"line_items"`
	TaxLines      []interface{} `json:"tax_lines"`
	ShippingLines []struct {
		ID          int           `json:"id"`
		MethodTitle string        `json:"method_title"`
		MethodID    string        `json:"method_id"`
		InstanceID  string        `json:"instance_id"`
		Total       string        `json:"total"`
		TotalTax    string        `json:"total_tax"`
		Taxes       []interface{} `json:"taxes"`
		MetaData    []struct {
			ID    int    `json:"id"`
			Key   string `json:"key"`
			Value string `json:"value"`
		} `json:"meta_data"`
	} `json:"shipping_lines"`
	FeeLines    []interface{} `json:"fee_lines"`
	CouponLines []interface{} `json:"coupon_lines"`
	Refunds     []interface{} `json:"refunds"`
	Links       struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
		Customer []struct {
			Href string `json:"href"`
		} `json:"customer"`
	} `json:"_links"`
}
