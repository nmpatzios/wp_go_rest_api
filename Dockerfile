FROM golang:latest AS builder
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && apt-get install -y --no-install-recommends apt-utils nodejs  libasound2-dev && npm install -g gulp

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . /app


RUN GOPATH=$(pwd):$(pwd)/vendor CGO_ENABLED=0 GOOS=linux go build -tags "dist" -ldflags "-s" -a -installsuffix cgo -o dist/wp-rest-api server
RUN cp -r src/server/static dist/static
RUN cp -r src/server/views dist/views
RUN cp src/server/BaltimoreCyberTrustRoot.crt.pem dist
RUN cp src/server/config.yaml dist/


FROM centurylink/ca-certs
WORKDIR /app
COPY --from=builder /app/dist .
EXPOSE 3003
CMD ["./wp-rest-api"]


