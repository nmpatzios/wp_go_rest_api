package main

import (
	"boltdb"
	"encoding/json"
	"models"
	"net/http"
	"printers"
	"strconv"
	"strings"
	"sync"
	"time"
	wc "woocommerce"

	"github.com/gorilla/mux"
	logurus "github.com/sirupsen/logrus"
)

var Limit int
var DefaultForm string
var GetLastOrderID int

func init() {

	db, err := boltdb.SetupDB()
	if err != nil {
		logurus.Error(err.Error())

	}
	defer db.Close()

	Limit, err = boltdb.GtTotalCharacters(db, Limit)

	if err != nil {
		logurus.Error(err.Error())
		err = boltdb.SetTotalCharacters(db, 50)
		if err != nil {
			logurus.Error(err.Error())

		}

	}

	DefaultForm, err = boltdb.GetCusstomForm(db, DefaultForm)

	if err != nil {

		logurus.Error(err.Error())

		DefaultForm = printTmpl
	} else {
		DefaultForm = DefaultForm
	}
	_, err = boltdb.GetLastOrderID(db, GetLastOrderID)

	if err != nil {

		logurus.Error(err.Error())
		err = boltdb.SetLastOrderID(db, 0)
		if err != nil {
			logurus.Error(err.Error())

		}
	}

	customer, err := boltdb.GetCusotmerKeySecret(db, &customerKeysecret)

	if err != nil {
		logurus.Error(err.Error())
	}

	client, err = wc.NewClient(
		customer.WPDomain,
		customer.Key,
		customer.Secret,
		&wc.Options{
			API:     true,
			Version: "/wp-json/wc/v3",
		},
	)

	if err != nil {
		logurus.Error(err.Error())
	}

}

var waitgroup sync.WaitGroup

func UpdateStatusJSON(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var orderStatus models.OrderStatus

	err := decoder.Decode(&orderStatus)
	if err != nil {
		logurus.Error(err.Error())
	}

	order, err := client.ChangeOrderStatus(orderStatus.Status, orderStatus.ID)

	if err != nil {
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Failed to update order with id : " + strconv.Itoa(int(order.ID))})
		return
	}

	t := time.Now()
	go orders(t)

	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Successfully changed status from : " + orderStatus.Status + " to " + order.Status + " for id: " + strconv.Itoa(int(order.ID))})
}

func UpdateProducts(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var productStatus []wc.ProductsStatus

	err := decoder.Decode(&productStatus)
	if err != nil {
		logurus.Error(err.Error())
	}

	client.ChangeProductStatus(productStatus)

	// if err != nil {
	// 	json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "Failed to update product "})
	// 	return
	// }

	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Successfully changed status"})
}
func Tiposi(w http.ResponseWriter, r *http.Request) {

	// vars := mux.Vars(r)
	// ID, err := strconv.Atoi(vars["id"])
	// if err != nil {
	// 	println(err.Error())
	// }

	decoder := json.NewDecoder(r.Body)
	var orderNote interface{}

	err := decoder.Decode(&orderNote)
	if err != nil {
		logurus.Error(err.Error())
	}
	inrec, _ := json.Marshal(orderNote)
	go printers.SendToPrinter(inrec, Limit, DefaultForm)
	// fmt.Println(orderNote)

	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Successfully printed "})
}

func SettoDefaultForm(w http.ResponseWriter, r *http.Request) {

	DefaultForm = printTmpl

	if DefaultForm != printTmpl {
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "Failed to change to default form "})
		return
	}

	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Successfully Changed to default from "})
}

func CheckIfAdmin(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["adminpass"])
	if err != nil {
		logurus.Error(err.Error())
	}

	admnPass, _ := strconv.Atoi(models.DecryptPass(models.KeyGenerator, c.ADMINPASS))

	if ID != admnPass {

		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "Wrong password "})
		return

	}
	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Hi, admin "})

}

func TotalCharacters(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		logurus.Error(err.Error())
	}
	// println(ID)

	db, err := boltdb.SetupDB()
	if err != nil {
		logurus.Error(err.Error())
	}
	defer db.Close()

	err = boltdb.SetTotalCharacters(db, ID)

	if err != nil {
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "error : " + err.Error()})
		logurus.Error(err.Error())
		return

	}
	Limit, err = boltdb.GtTotalCharacters(db, Limit)

	if err != nil {
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "error : " + err.Error()})
		logurus.Error(err.Error())
		return

	}

	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Successfully changed total characters "})
}

func FormatOrder(w http.ResponseWriter, r *http.Request) {
	type Format struct {
		Text string `jon:"text"`
	}

	var format Format

	decoder := json.NewDecoder(r.Body)

	err := decoder.Decode(&format)
	if err != nil {
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "error : " + err.Error()})
		logurus.Error(err.Error())
		return

	}

	results := strings.Replace(format.Text, "{{ range .LineItems }}", `{{range .LineItems}}{{.Product}}{{range .MetaData}}{{ range  $value := arrays .Value }}
{{trim $value}}{{end}}{{end}}
{{end}}`, -1)
	moreformat := strings.Replace(results, "{{ .TotalCount }}", `{{ $length := len .LineItems }}{{$length}}`, -1)

	// fmt.Printf("%s", moreformat)

	db, err := boltdb.SetupDB()
	if err != nil {
		logurus.Error(err.Error())
	}
	defer db.Close()

	err = boltdb.SetCusstomForm(db, moreformat)
	DefaultForm = moreformat

	if err != nil {
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "error : " + err.Error()})
		logurus.Error(err.Error())
		return

	}

	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Successfully changed total characters "})
}

func CreateOrderNoteJSON(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var orderNote models.OrderNote

	err := decoder.Decode(&orderNote)
	if err != nil {
		logurus.Error(err.Error())
	}

	note, err := client.CreateOrderNote(orderNote.Message, orderNote.ID)

	if err != nil {
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "Αποτυχία ενημέρωσεις παραγγελίας με Αριθμο : " + orderNote.ID + "error : " + err.Error()})
		logurus.Error(err.Error())

		return
	}
	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Η παραγγελία με αριθμό  : " + orderNote.ID + " ενημερώθηκε με επιτυχία.", Body: note.Note})
}
func Totals(w http.ResponseWriter, r *http.Request) {

	var totals wc.TotalsAmount

	totals.Total = client.GeTotals()
	totalamounts := client.GetTotalAMounts(activeAt)

	// fmt.Println(totalamounts[0].Totals)

	totals.TotalAmount = totalamounts[0].TotalSales

	now := client.GetTotalAMountsPerDay()
	totals.TotalToday = now[0].TotalSales

	json.NewEncoder(w).Encode(totals)
}

func InsertCustomerKeySecret(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var customerKeysecret boltdb.CustomerKeysecret

	err := decoder.Decode(&customerKeysecret)
	if err != nil {
		logurus.Error(err.Error())
	}

	db, err := boltdb.SetupDB()
	if err != nil {
		logurus.Error(err.Error())
	}
	defer db.Close()

	customerKeysecret.ID = 1

	err = boltdb.SetCustomerKeySecret(db, customerKeysecret)

	if err != nil {
		logurus.Error(err.Error())
	}

	if err != nil {
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "error : " + err.Error()})
		logurus.Error(err.Error())

		return
	}

	client, err = wc.NewClient(
		customerKeysecret.WPDomain,
		customerKeysecret.Key,
		customerKeysecret.Secret,
		&wc.Options{
			API:     true,
			Version: "/wp-json/wc/v3",
		},
	)

	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200})
}

func GetSales(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	from, _ := strconv.ParseInt(vars["from"], 10, 64)
	fromt := time.Unix(from, 0)
	to, _ := strconv.ParseInt(vars["to"], 10, 64)
	tot := time.Unix(to, 0)

	var totalrange []models.MyTotalsRange
	var totalsingle models.TotalsRange

	totalamounts := client.GetTotalAMountsCustomRange(fromt.Format("2006-01-02"), tot.Format("2006-01-02"))

	diff := tot.Sub(fromt)

	// fmt.Println(int(diff.Hours() / 24)) // number of days

	if int(diff.Hours()/24) > 300 {

		for key, price := range totalamounts[0].Totals {
			b, _ := json.Marshal(price)

			err := json.Unmarshal(b, &totalsingle)
			if err != nil {
				logurus.Error(err.Error())
			}

			var single models.MyTotalsRange
			single.Customers = totalsingle.Customers
			single.Date = key
			single.Discount = totalsingle.Discount
			single.Items = totalsingle.Items
			single.Orders = totalsingle.Orders
			single.Sales = totalsingle.Sales
			single.Shipping = totalsingle.Shipping
			single.Tax = totalsingle.Tax
			totalrange = append(totalrange, single)

		}

	} else {

		for key, price := range totalamounts[0].Totals {
			b, _ := json.Marshal(price)

			err := json.Unmarshal(b, &totalsingle)
			if err != nil {
				logurus.Error(err.Error())
			}
			var single models.MyTotalsRange
			single.Customers = totalsingle.Customers
			single.Date = key
			single.Discount = totalsingle.Discount
			single.Items = totalsingle.Items
			single.Orders = totalsingle.Orders
			single.Sales = totalsingle.Sales
			single.Shipping = totalsingle.Shipping
			single.Tax = totalsingle.Tax
			totalrange = append(totalrange, single)

		}
	}

	json.NewEncoder(w).Encode(totalrange)

}

func monthsCountSince(createdAtTime time.Time) int {
	now := time.Now()
	months := 0
	month := createdAtTime.Month()
	for createdAtTime.Before(now) {
		createdAtTime = createdAtTime.Add(time.Hour * 24)
		nextMonth := createdAtTime.Month()
		if nextMonth != month {
			months++
		}
		month = nextMonth
	}

	return months
}

func BuyCredits(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var customer wc.WPCustomer

	err := decoder.Decode(&customer)
	if err != nil {
		logurus.Error(err.Error())
		json.NewEncoder(w).Encode(models.HTTPResp{Status: 500, Description: "Failed to decode customer "})
		return
	}

	json.NewEncoder(w).Encode(models.HTTPResp{Status: 200, Description: "Success"})
}
