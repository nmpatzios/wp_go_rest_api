package boltdb

import "time"

// Config type
type Config struct {
	Height   float64   `json:"height"`
	Birthday time.Time `json:"birthday"`
}

// Entry type
type Entry struct {
	Calories int    `json:"calories"`
	Food     string `json:"food"`
}

type CustomerKeysecret struct {
	ID       int    `json:"id"`
	Key      string `json:"customer_key"`
	Secret   string `json:"customer_secret"`
	WPDomain string `json:"wp_domain"`
	WPAPI    string `json:"wp_api"`
}
