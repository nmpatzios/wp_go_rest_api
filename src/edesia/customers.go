package edesia

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	wc "woocommerce"
)

func CheckIfEdesiaCustomerExists(customerEmail string) ([]wc.WPCustomer, error) {
	client, err := GetClient()
	var customer []wc.WPCustomer

	req, err := http.NewRequest("GET", fmt.Sprintf("%scustomers", client.StoreURL), nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()

	q.Add("consumer_key", client.Ck)
	q.Add("consumer_secret", client.Cs)
	q.Add("email", customerEmail)

	req.URL.RawQuery = q.Encode()

	err = GetJson(req.URL.String(), &customer)
	if err != nil {
		return nil, err

	}

	return customer, nil
}

func CreateCustomer(jsonMarshall interface{}) (CreateResponseCustomer, error) {
	c, err := GetClient()
	if err != nil {
		return CreateResponseCustomer{}, err

	}

	var customerResponse CreateResponseCustomer
	var createEdesiaCustomer CreateEdesiaCustomer
	var customer wc.WPCustomer
	bb, _ := json.Marshal(jsonMarshall)
	json.Unmarshal(bb, &customer)

	customer.Username = "nikosbadtziosgs"

	customer.Email = "nikosbatziiosgsd@gmail.com"
	customer.BillingAddress.Email = "nikosbatziiodsg@gmail.com"

	createEdesiaCustomer.Email = customer.Email
	createEdesiaCustomer.FirstName = customer.FirstName
	createEdesiaCustomer.LastName = customer.LastName
	createEdesiaCustomer.Username = customer.Username
	createEdesiaCustomer.Billing = customer.BillingAddress
	createEdesiaCustomer.Shipping = customer.ShippingAddress

	body, err := c.Post("customers", createEdesiaCustomer)

	jsonDecoder := json.NewDecoder(body)

	if err := jsonDecoder.Decode(&customerResponse); err != nil && err != io.EOF {
		return CreateResponseCustomer{}, err
	}
	defer body.Close()

	fmt.Println(customerResponse)
	return customerResponse, nil
}
