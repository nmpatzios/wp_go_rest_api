package edesia

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	wc "woocommerce"
)

func GetProducts() ([]wc.Product, error) {
	client, err := GetClient()

	// v := url.Values{}
	var data []wc.Product
	req, err := http.NewRequest("GET", fmt.Sprintf("%sproducts", client.StoreURL), nil)
	if err != nil {
		return []wc.Product{}, err
	}

	q := req.URL.Query()

	q.Add("consumer_key", client.Ck)
	q.Add("consumer_secret", client.Cs)
	req.URL.RawQuery = q.Encode()

	err = GetJson(req.URL.String(), &data)
	if err != nil {
		return []wc.Product{}, err

	}

	return data, nil

}

func CreateProduct(product CreateProductRequest) (CreateProductResponse, error) {
	c, err := GetClient()
	if err != nil {
		return CreateProductResponse{}, err

	}

	body, err := c.Post("products", product)

	jsonDecoder := json.NewDecoder(body)

	var createProductResponse CreateProductResponse

	if err := jsonDecoder.Decode(&createProductResponse); err != nil && err != io.EOF {
		return CreateProductResponse{}, err
	}
	defer body.Close()

	return createProductResponse, nil
}
