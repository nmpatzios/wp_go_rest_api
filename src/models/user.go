package models

import (
	"time"
	"woocommerce"
	wc "woocommerce"
)

var PackagesProducts *woocommerce.Products

type Users struct {
	Active                      string
	Title                       string
	Username                    string
	Role                        string
	MenuTitle                   string
	MessageError                string
	Users                       []User
	User                        *WPLoginUser
	MetaUser                    WPUserMeta
	JSONMetaUser                string
	Orders                      []wc.Order
	Order                       wc.Order
	P                           P
	Customers                   []woocommerce.WPCustomer
	Customer                    woocommerce.WPCustomer
	OrdersPerCustomer           []woocommerce.OrdersPerCustomer
	Products                    []woocommerce.Product
	Categories                  []*woocommerce.ProductCategory
	Product                     woocommerce.Product
	SelectedCategories          []string
	SelectAll                   bool
	WPAPI                       bool
	Host                        string
	TotalPages                  int
	TotalProducts               int
	Search                      string
	TotalOdersPerCustomer       int
	TotalCancelOdersPerCustomer int
	TotalOdersPerCustomerMoney  float64
	Shipping                    string
}
type P struct {
	Data    string
	LastMod string
	Host    string
}
type WPUserMeta struct {
	Status                            string `json:"status"`
	FirstName                         string `json:"first_name"`
	LastName                          string `json:"last_name"`
	Nickname                          string `json:"nickname"`
	RichEditing                       string `json:"rich_editing"`
	CommentShortcuts                  string `json:"comment_shortcuts"`
	AdminColor                        string `json:"admin_color"`
	Se3bCapabilities                  string `json:"se3b_capabilities"`
	Se3bUserLevel                     string `json:"se3b_user_level"`
	SessionTokens                     string `json:"session_tokens"`
	Se3bDashboardQuickPressLastPostID string `json:"se3b_dashboard_quick_press_last_post_id"`
	CommunityEventsLocation           string `json:"community-events-location"`
	DismissedWpPointers               string `json:"dismissed_wp_pointers"`
	DismissedNoSecureConnectionNotice string `json:"dismissed_no_secure_connection_notice"`
	Se3bUserSettings                  string `json:"se3bUserSettings"`
	Se3bUserSettingsTime              string `json:"se3b_user-settings-time"`
	WcLastActive                      string `json:"wc_last_active"`
	SyntaxHighlighting                string `json:"syntax_highlighting"`
	ShowAdminBarFront                 string `json:"show_admin_bar_front"`
	LastUpdate                        string `json:"last_update"`
	BillingFirstName                  string `json:"billing_first_name"`
	BillingLastName                   string `json:"billing_last_name"`
	BillingCompany                    string `json:"billing_company"`
	BillingAddress1                   string `json:"billing_address_1"`
	BillingAddress2                   string `json:"billing_address_2"`
	BillingCity                       string `json:"billing_city"`
	BillingState                      string `json:"billing_state"`
	BillingPostcode                   string `json:"billing_postcode"`
	BillingCountry                    string `json:"billing_country"`
	BillingEmail                      string `json:"billing_email"`
	BillingPhone                      string `json:"billing_phone"`
	ShippingFirstName                 string `json:"shipping_first_name"`
	ShippingLastName                  string `json:"shipping_last_name"`
	ShippingCompany                   string `json:"shipping_company"`
	ShippingAddress1                  string `json:"shipping_address_1"`
	ShippingAddress2                  string `json:"shipping_address_2"`
	ShippingCity                      string `json:"shipping_city"`
	ShippingState                     string `json:"shipping_state"`
	ShippingPostcode                  string `json:"shipping_postcode"`
	ShippingCountry                   string `json:"shipping_country"`
	ShippingMethod                    string `json:"shipping_method"`
	WoocommercePersistentCart1        string `json:"_woocommerce_persistent_cart_1"`
	PayingCustomer                    string `json:"paying_customer"`
	OrderCount                        string `json:"_order_count"`
	MoneySpent                        string `json:"_money_spent"`
}

//User holds the data for each user
type User struct {
	ID               int       `json:"id"`
	Username         string    `json:"username"`
	Afm              string    `json:"afm"`
	Email            string    `json:"email"`
	Password         string    `json:"password"`
	Forename         string    `json:"forename"`
	Surname          string    `json:"surname"`
	DistinctiveTitle string    `json:"distinctive_title"`
	TelephoneNumber  string    `json:"telephone_number"`
	Profession       string    `json:"profession"`
	Doy              string    `json:"doy"`
	Address          string    `json:"address"`
	City             string    `json:"city"`
	State            string    `json:"state"`
	ZipCode          string    `json:"zip_code"`
	Country          string    `json:"country"`
	IsActive         bool      `json:"is_active"`
	CreatedAt        time.Time `json:"created_at"`
	UpdatedAt        time.Time `json:"updated_at"`
	LastLogin        time.Time `json:"last_login"`
}

type WPLoginUser struct {
	Status     string `json:"status"`
	Cookie     string `json:"cookie"`
	CookieName string `json:"cookie_name"`
	User       WPUser `json:"user"`
}
type WPUser struct {
	ID        int    `json:"id,omitempty"`
	AvatarURL string `json:"avatar_url,omitempty"`
	// AvatarURLs        AvatarURLS             `json:"avatar_urls,omitempty"`
	// Capabilities      map[string]interface{} `json:"capabilities,omitempty"`
	Description string `json:"description,omitempty"`
	Email       string `json:"email,omitempty"`
	// ExtraCapabilities map[string]interface{} `json:"extra_capabilities,omitempty"`
	FirstName      string   `json:"firstname,omitempty"`
	LastName       string   `json:"lastname,omitempty"`
	Link           string   `json:"link,omitempty"`
	Name           string   `json:"name,omitempty"`
	Nickname       string   `json:"nickname,omitempty"`
	DisplayedName  string   `json:"displayname,omitempty"`
	RegisteredDate string   `json:"registered,omitempty"`
	Roles          []string `json:"roles,omitempty"`
	Slug           string   `json:"slug,omitempty"`
	URL            string   `json:"url,omitempty"`
	Username       string   `json:"username,omitempty"`
	Password       string   `json:"password,omitempty"`
	Nicename       string   `json:"nicename,omitempty"`
}

type HTTPResp struct {
	Status      int    `json:"status"`
	Description string `json:"description"`
	Body        string `json:"body"`
}

type OrderStatus struct {
	ID     int    `json:"id"`
	Status string `json:"status"`
}

type OrderNote struct {
	ID      string `json:"id"`
	Message string `json:"message"`
}

type CustomerKeysecret struct {
	Key    string `json:"customer_key"`
	Secret string `json:"customer_secret"`
}
