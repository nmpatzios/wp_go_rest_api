package woocommerce

import (
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{}
var orderConnections []*websocket.Conn
var logch chan string

var chcounter chan int

func GetWebsocket(w http.ResponseWriter, r *http.Request) {
	conn, _ := upgrader.Upgrade(w, r, nil)

	orderConnections = append(orderConnections, conn)

	go func(c *websocket.Conn) {
		for {
			if _, _, err := c.NextReader(); err != nil {
				c.Close()

				for i := range orderConnections {
					if orderConnections[i] == c {
						orderConnections = append(orderConnections[:i], orderConnections[i+1:]...)
					}
				}
				break
			}
		}
	}(conn)
}

func init() {

	logch = make(chan string)
	go func() {
		for msg := range logch {
			for _, c := range orderConnections {

				w, _ := c.NextWriter(websocket.TextMessage)
				// w.Write([]byte(time.Now().Format("2006-01-02 15:04:05")))
				w.Write([]byte(msg))
				w.Close()
			}
		}
	}()

}
