package main

import (
	"boltdb"
	"context"
	"edesia"
	"encoding/json"
	"fmt"
	"html/template"
	"models"
	"net/http"
	"strings"
	"time"
	wc "woocommerce"

	logurus "github.com/sirupsen/logrus"
)

var user1 models.WPLoginUser
var activeAt string

var loginTemplate = template.Must(template.New("login.html").Funcs(templateFuncMap).ParseFiles("views/login.html"))
var activateTemplate = template.Must(template.New("activate.html").Funcs(templateFuncMap).ParseFiles("views/activate.html"))

func Auth(users *models.User, password string, w http.ResponseWriter, r *http.Request) {

	setSession(users.Afm, w)
	http.Redirect(w, r, "/admin", 302)
}

func Admin(w http.ResponseWriter, r *http.Request) {

	context := GetIndex()
	cookie := getUserName(r)

	if cookie == "o" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	context.User = &user1
	b, err := json.Marshal(user1)
	if err != nil {
		fmt.Println(err)
		return
	}

	// context.MetaUser = metaUser
	context.JSONMetaUser = string(b)

	// var metaUser models.WPUserMeta
	// err := GetJson(url1, &metaUser)
	// if err != nil {
	// 	println(err.Error())
	// }
	// fmt.Printf("Req: %s %s\n", r.Host, r.URL.Path)
	context.Host = r.Host

	t := time.Now()
	go orders(t)

	renderView("index.html", w, context)

}

func Index(w http.ResponseWriter, r *http.Request) {
	context := GetLogin()
	fmt.Println(context.MessageError)

	http.Redirect(w, r, "/admin", 301)
}

// AdminCheck check render the static content for the logged in user
func AdminCheck(next http.Handler, redirect bool) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		wordpress := getUserName(r)
		if len(wordpress) != 0 {
			ctx := context.WithValue(r.Context(), "wordpress", wordpress)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			if redirect {
				http.Redirect(w, r, "/admin/login", http.StatusTemporaryRedirect)
				return
			}
			http.Error(w, "Forbidden", http.StatusForbidden)
		}

	})
}

var erromessage *string

func login(w http.ResponseWriter, r *http.Request) {

	context := GetLogin()

	activeuser, err := CheckIfisActiveOrExist()
	if err != nil {
		context.MessageError = `<div class='alert alert-danger alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
								</button>
								<strong>This executable cannot run on this machine, please call NIKOLAS NTEKAS or email info@pixelhub.eu to purchase new license
							</div>`
		err := loginTemplate.Execute(w, context)

		if err != nil {
			logurus.Error(err.Error())
		}
		return
	}

	if activeuser.IsActive == false {
		logurus.Error("This executable cannot run on this machine, please call 11223344 or email support@company.com to purchase new license")

		context.MessageError = `<div class='alert alert-danger alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
								</button>
								<strong>This executable cannot run on this machine, please call NIKOLAS NTEKAS or email info@pixelhub.eu to purchase new license
							</div>`
		err := loginTemplate.Execute(w, context)

		if err != nil {
			logurus.Error(err.Error())
		}
		return
	}
	dbbolt, err := boltdb.SetupDB()
	if err != nil {
		logurus.Error(err.Error())
	}
	defer dbbolt.Close()

	customer, err := boltdb.GetCusotmerKeySecret(dbbolt, &customerKeysecret)

	if err != nil {
		logurus.Error(err.Error())
	}

	if customer.WPAPI != "" {
		context.WPAPI = true
	}

	if erromessage != nil {
		context.MessageError = *erromessage

	}

	err = loginTemplate.Execute(w, context)

	if err != nil {
		logurus.Error(err.Error())
	}

}

func GetWPUser(api, username, password string) (*models.WPLoginUser, error) {
	url := fmt.Sprintf("%s/user/generate_auth_cookie/?username=%s&password=%s", api, username, password)
	// println(url)
	// println(user1.Cookie)

	err := GetJson(url, &user1)
	if err != nil {
		return nil, err

	}
	return &user1, nil
}
func loginPost(w http.ResponseWriter, r *http.Request) {
	dbbolt, err := boltdb.SetupDB()
	if err != nil {
		logurus.Error(err.Error())
	}
	defer dbbolt.Close()

	context := GetLogin()

	username := r.FormValue("afm")
	password := r.FormValue("password")
	wpapi := r.FormValue("wp_api")
	ck := r.FormValue("ck")
	cs := r.FormValue("cs")
	wpdomain := r.FormValue("wp_domain")
	apibool := r.FormValue("wpapi")

	getWPUser, err := GetWPUser(wpapi, username, password)
	if err != nil {
		logurus.Error(err.Error())
	}

	edesiaCustomerExist, err := edesia.CheckIfEdesiaCustomerExists(getWPUser.User.Email)
	if err != nil {
		context.MessageError = `<div class='alert alert-danger alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
								</button>
								<strong>Cannot  Check for excisting customer: ` + err.Error() + `
							</div>`
		err := loginTemplate.Execute(w, context)

		if err != nil {
			logurus.Error(err.Error())
		}
		return
	}
	jsonMarshall := client.SingleCustomer(getWPUser.User.ID)

	if len(edesiaCustomerExist) == 0 {

		customerKeysecret.WPAPI = wpapi
		customerKeysecret.WPDomain = wpdomain
		customerKeysecret.Key = ck
		customerKeysecret.Secret = cs
		customerKeysecret.ID = 1

		err = boltdb.SetCustomerKeySecret(dbbolt, customerKeysecret)
		if err != nil {
			logurus.Error(err.Error())
		}

		customer, err := boltdb.GetCusotmerKeySecret(dbbolt, &customerKeysecret)

		if err != nil {
			logurus.Error(err.Error())
		}

		client, err = wc.NewClient(
			customer.WPDomain,
			customer.Key,
			customer.Secret,
			&wc.Options{
				API:     true,
				Version: "/wp-json/wc/v3",
			},
		)

		if err != nil {
			logurus.Error(err.Error())
		}

		s, err := edesia.CreateCustomer(jsonMarshall)
		if err != nil {
			logurus.Error(err.Error())
		}

		var createProduct edesia.CreateProductRequest
		createProduct.Name = fmt.Sprintf("%s_%v_%s", s.Username, s.ID, GenerateLicenseKey())
		createProduct.Type = "simple"
		createProduct.RegularPrice = "1"
		createProduct.ShortDescription = fmt.Sprintf("%s %s", s.FirstName, s.LastName)
		createProductReponse, err := edesia.CreateProduct(createProduct)

		if err != nil {
			logurus.Error(err.Error())
		}

		fmt.Printf("\n\n\n %+v\n\n\n", createProductReponse)
	}

	var newCustomer wc.WPCustomer
	bb, _ := json.Marshal(jsonMarshall)
	json.Unmarshal(bb, &newCustomer)

	allEdesisProducts, err := edesia.GetProducts()
	if err != nil {
		logurus.Error(err.Error())
		return
	}

	// compareKey := GenerateLicenseKey()

	for _, p := range allEdesisProducts {
		productName := strings.Split(p.Name, "_")
		println(len(productName))
		// if len(productName) > 1 && productName[2] == compareKey {
		// 	println("found")
		// 	return
		// }

	}

	return

	activeuser, err := CheckIfisActiveOrExist()
	if err != nil {
		context.MessageError = `<div class='alert alert-danger alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
								</button>
								<strong>This executable cannot run on this machine, please call NIKOLAS NTEKAS or email info@pixelhub.eu to purchase new license
							</div>`
		err := loginTemplate.Execute(w, context)

		if err != nil {
			logurus.Error(err.Error())
		}
		return
	}

	if activeuser.IsActive == false {
		logurus.Error("This executable cannot run on this machine, please call 11223344 or email support@company.com to purchase new license")

		context.MessageError = `<div class='alert alert-danger alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
								</button>
								<strong>This executable cannot run on this machine, please call NIKOLAS NTEKAS or email info@pixelhub.eu to purchase new license
							</div>`
		err := loginTemplate.Execute(w, context)

		if err != nil {
			logurus.Error(err.Error())
		}
		return
	}

	if apibool == "false" {
		customerKeysecret.WPAPI = wpapi
		customerKeysecret.WPDomain = wpdomain
		customerKeysecret.Key = ck
		customerKeysecret.Secret = cs
		customerKeysecret.ID = 1

		err = boltdb.SetCustomerKeySecret(dbbolt, customerKeysecret)
		if err != nil {
			logurus.Error(err.Error())
		}
	}

	customer, err := boltdb.GetCusotmerKeySecret(dbbolt, &customerKeysecret)

	if err != nil {
		logurus.Error(err.Error())
	}

	if customer.WPAPI == "" {
		context.MessageError = `<div class='alert alert-danger alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
								</button>
								<strong>Please provide WP API
							</div>`
		err := loginTemplate.Execute(w, context)

		if err != nil {
			logurus.Error(err.Error())
		}

	}

	client, err = wc.NewClient(
		customer.WPDomain,
		customer.Key,
		customer.Secret,
		&wc.Options{
			API:     true,
			Version: "/wp-json/wc/v3",
		},
	)

	if err != nil {
		logurus.Error(err.Error())
	}

	url := fmt.Sprintf("%s/user/generate_auth_cookie/?username=%s&password=%s", customer.WPAPI, username, password)
	// println(url)
	// println(user1.Cookie)

	err = GetJson(url, &user1)
	if err != nil {
		logurus.Error(err.Error())
		context.MessageError = `<div class='alert alert-danger alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
								</button>
								<strong>` + err.Error() + `</strong>
							</div>`
		err := loginTemplate.Execute(w, context)

		if err != nil {
			logurus.Error(err.Error())
		}
	}

	if user1.Status == "ok" {
		setSession(user1.Cookie, w)
		times, err := time.Parse("2006-01-02 5:04:05", user1.User.RegisteredDate)
		if err != nil {
			logurus.Error(err.Error())
		}
		activeAt = times.Format("2006-01-02")
		http.Redirect(w, r, "/admin", 302)

		return

	}

	strPointer := `<div class='alert alert-danger alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
								</button>
								<strong>Παρακαλώ δοκιμάστε ξανά ,Λαθος όνομα χρήστη ή κωδικός
							</div>`

	// err = loginTemplate.Execute(w, context)
	// if err != nil {
	// 	logurus.Error(err.Error())
	// }

	erromessage = &strPointer

	w.Header().Set("EROR-VALUE", "Παρακαλώ δοκιμάστε ξανά ,Λαθος όνομα χρήστη ή κωδικός")
	http.Redirect(w, r, "/admin", 302)

}
func logout(w http.ResponseWriter, r *http.Request) {

	clearSession(w)
	http.Redirect(w, r, "/admin/login", 302)
}

// const (
// 	apiurl       = "https://api2.smsmobile.gr/receiver_post.php"
// 	userdetailts = "SuccSteps:SucS3ps236"
// )

func GetJson(url string, target interface{}) error {
	r, err := http.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}
