package models

type TotalsRange struct {
	Customers int    `json:"customers"`
	Discount  string `json:"discount"`
	Items     int    `json:"items"`
	Orders    int    `json:"orders"`
	Sales     string `json:"sales"`
	Shipping  string `json:"shipping"`
	Tax       string `json:"tax"`
}

type MyTotalsRange struct {
	Customers int    `json:"customers"`
	Discount  string `json:"discount"`
	Items     int    `json:"items"`
	Orders    int    `json:"orders"`
	Sales     string `json:"sales"`
	Shipping  string `json:"shipping"`
	Tax       string `json:"tax"`
	Date      string `json:"date"`
}
