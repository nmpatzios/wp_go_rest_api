package main

import (
	"models"
	"os"

	logurus "github.com/sirupsen/logrus"
)

func GetIndex() models.Users {
	var result models.Users

	result.Active = "index"
	result.Title = "Αρχική"
	result.MenuTitle = "Σύνοψη"
	return result
}

func GetLogin() models.Users {
	var result models.Users

	result.Active = "login"
	result.Title = "Login"

	return result
}

func GetOrders() models.Users {
	var result models.Users

	result.Active = "orders"
	result.Title = "Nέες Παραγγελίες"

	return result
}

func GetCompletedOrders() models.Users {
	var result models.Users

	result.Active = "completedorders"
	result.Title = "Ολοκληρωμένες Παραγγελίες"

	return result
}
func GetPricingTablePage() models.Users {
	var result models.Users

	result.Active = "pricingtable"
	result.Title = "Pricing Tables"

	return result
}

func GetCustomerForm() models.Users {
	var result models.Users

	result.Active = "customerform"
	result.Title = ""

	return result
}

func GetCancelledOrders() models.Users {
	var result models.Users

	result.Active = "cancelledorders"
	result.Title = "Ακυρωμένες Παραγγελίες"

	return result
}

func GetCustomers() models.Users {
	var result models.Users

	result.Active = "customers"
	result.Title = "Customers"

	return result
}

func GetProducts() models.Users {
	var result models.Users

	result.Active = "products"
	result.Title = "Products"

	return result
}

func GetSettingsPage() models.Users {
	var result models.Users

	result.Active = "settings"
	result.Title = "Ρυθμίσεις"

	return result
}

func GetPrinterSettingsPage() models.Users {
	var result models.Users

	result.Active = "printersettings"
	result.Title = "Ρυθμίσεις Εκτυπωτή"

	return result
}

func Get404() models.Users {
	var result models.Users
	result.Active = "404"
	result.Title = "Page not found"

	return result
}
func CreateDirIfNotExist(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			logurus.Error(err.Error())
		}
	}
}
