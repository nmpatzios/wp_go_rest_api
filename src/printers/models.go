package printers

import "strings"

type Order struct {
	ID                 int            `json:"id"`
	ParentID           int            `json:"parent_id"`
	Number             string         `json:"number"`
	OrderKey           string         `json:"order_key"`
	CreatedVia         string         `json:"created_via"`
	Versions           string         `json:"version"`
	Status             string         `json:"status"`
	Currency           string         `json:"currency"`
	DateCreated        string         `json:"date_created"`
	DateCreatedGMT     string         `json:"date_created_gmt"`
	DateModified       string         `json:"date_modified"`
	DateModifiedGMT    string         `json:"date_modified_gmt"`
	DiscountTotal      string         `json:"discount_total"`
	DiscountTax        string         `json:"discount_tax"`
	ShippingTotal      string         `json:"shipping_total"`
	ShippingTax        string         `json:"shipping_tax"`
	CartTax            string         `json:"cart_tax"`
	Total              string         `json:"total"`
	TotalTax           string         `json:"total_tax"`
	PricesIncludeTax   bool           `json:"prices_include_tax"`
	CustomnerID        int            `json:"customer_id"`
	CustomerIPAddress  string         `json:"customer_ip_address"`
	CustomerUserAgent  string         `json:"customer_user_agent"`
	CustomerNote       string         `json:"customer_note"`
	Billing            Billing        `json:"billing"`
	Shipping           Shipping       `json:"shipping"`
	PaymentMethod      string         `json:"payment_method"`
	PaymentMethodTitle string         `json:"payment_method_title"`
	TransactionID      string         `json:"transaction_id"`
	DatePaid           string         `json:"date_paid"`
	DatePaidGMT        string         `json:"date_paid_gmt"`
	DateCompleted      string         `json:"date_completed"`
	DateCompletedGMT   string         `json:"date_completed_gmt"`
	CardHash           string         `json:"cart_hash"`
	MetaData           interface{}    `json:"meta_data"`
	LineItems          []LineItem     `json:"line_items"`
	TaxLines           interface{}    `json:"tax_lines"`
	ShippingLines      []ShippingLine `json:"shipping_lines"`
	FeeLines           interface{}    `json:"fee_lines"`
	CouponLines        interface{}    `json:"coupon_lines"`
	Refunds            interface{}    `json:"refunds"`
	Links              Links          `json:"_links"`
}

type Billing struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	City      string `json:"city"`
	State     string `json:"state"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
}
type Shipping struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	City      string `json:"city"`
	State     string `json:"state"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
}
type Links struct {
	Self       []Self       `json:"self"`
	Collection []Collection `json:"collection"`
	Customer   []Customer   `json:"customer"`
}
type Self struct {
	Href string `json:"href"`
}
type Collection struct {
	Href string `json:"href"`
}
type Customer struct {
	Href string `json:"href"`
}

type LineItem struct {
	ID          int                `json:"id"`
	Name        string             `json:"name"`
	ProductID   int                `json:"product_id"`
	VariationID int                `json:"variation_id"`
	Quantity    int                `json:"quantity"`
	TaxClass    string             `json:"tax_class"`
	Subtotal    string             `json:"subtotal"`
	SubTotalTax string             `json:"subtotal_tax"`
	Total       string             `json:"total"`
	TotalTax    string             `json:"total_tax"`
	Taxes       interface{}        `json:"taxes"`
	MetaData    []LineItemMetaData `json:"meta_data"`
	SKU         string             `json:"sku"`
	Price       float64            `json:"price"`
	Product     string             `json:"-"`
}

type ShippingLine struct {
	ID          int                    `json:"id"`
	MethodTitle string                 `json:"method_title"`
	MethodID    string                 `json:"method_id"`
	InstanceID  string                 `json:"instance_id"`
	Total       string                 `json:"total"`
	TotalTax    string                 `json:"total_tax"`
	Taxes       interface{}            `json:"taxes"`
	MetaData    []ShippingLineMetaData `json:"meta_data"`
}
type ShippingLineMetaData struct {
	ID    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"value"`
}

type LineItemMetaData struct {
	ID    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"value"`
}

type WPCustomer struct {
	ID               int             `json:"id"`
	DateCreated      string          `json:"date_created"`
	DateCreatedGmt   string          `json:"date_created_gmt"`
	DateModified     string          `json:"date_modified"`
	DateModifiedGmt  string          `json:"date_modified_gmt"`
	Email            string          `json:"email"`
	FirstName        string          `json:"first_name"`
	LastName         string          `json:"last_name"`
	Role             string          `json:"role"`
	Username         string          `json:"username"`
	IsPayingCustomer bool            `json:"is_paying_customer"`
	OrdersCount      int             `json:"orders_count"`
	TotalSpent       string          `json:"total_spent"`
	AvatarURL        string          `json:"avatar_url"`
	BillingAddress   BillingAddress  `json:"billing"`
	ShippingAddress  ShippingAddress `json:"shipping"`
	MetaData         []struct {
		ID    int         `json:"id"`
		Key   string      `json:"key"`
		Value interface{} `json:"value"`
	} `json:"meta_data"`
	Links struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
	} `json:"_links"`
}

type BillingAddress struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	City      string `json:"city"`
	State     string `json:"state"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
}

type ShippingAddress struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	City      string `json:"city"`
	State     string `json:"state"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
}

type Products struct {
	Products []Product
}

type Product struct {
	ID                int           `json:"id"`
	Name              string        `json:"name"`
	Slug              string        `json:"slug"`
	Permalink         string        `json:"permalink"`
	DateCreated       string        `json:"date_created"`
	DateCreatedGmt    string        `json:"date_created_gmt"`
	DateModified      string        `json:"date_modified"`
	DateModifiedGmt   string        `json:"date_modified_gmt"`
	Type              string        `json:"type"`
	Status            string        `json:"status"`
	Featured          bool          `json:"featured"`
	CatalogVisibility string        `json:"catalog_visibility"`
	Description       string        `json:"description"`
	ShortDescription  string        `json:"short_description"`
	Sku               string        `json:"sku"`
	Price             string        `json:"price"`
	RegularPrice      string        `json:"regular_price"`
	SalePrice         string        `json:"sale_price"`
	DateOnSaleFrom    interface{}   `json:"date_on_sale_from"`
	DateOnSaleFromGmt interface{}   `json:"date_on_sale_from_gmt"`
	DateOnSaleTo      interface{}   `json:"date_on_sale_to"`
	DateOnSaleToGmt   interface{}   `json:"date_on_sale_to_gmt"`
	PriceHTML         string        `json:"price_html"`
	OnSale            bool          `json:"on_sale"`
	Purchasable       bool          `json:"purchasable"`
	TotalSales        int           `json:"total_sales"`
	Virtual           bool          `json:"virtual"`
	Downloadable      bool          `json:"downloadable"`
	Downloads         []interface{} `json:"downloads"`
	DownloadLimit     int           `json:"download_limit"`
	DownloadExpiry    int           `json:"download_expiry"`
	ExternalURL       string        `json:"external_url"`
	ButtonText        string        `json:"button_text"`
	TaxStatus         string        `json:"tax_status"`
	TaxClass          string        `json:"tax_class"`
	ManageStock       bool          `json:"manage_stock"`
	StockQuantity     interface{}   `json:"stock_quantity"`
	InStock           bool          `json:"in_stock"`
	Backorders        string        `json:"backorders"`
	BackordersAllowed bool          `json:"backorders_allowed"`
	Backordered       bool          `json:"backordered"`
	SoldIndividually  bool          `json:"sold_individually"`
	Weight            string        `json:"weight"`
	Dimensions        Dimensions    `json:"dimensions"`
	ShippingRequired  bool          `json:"shipping_required"`
	ShippingTaxable   bool          `json:"shipping_taxable"`
	ShippingClass     string        `json:"shipping_class"`
	ShippingClassID   int           `json:"shipping_class_id"`
	ReviewsAllowed    bool          `json:"reviews_allowed"`
	AverageRating     string        `json:"average_rating"`
	RatingCount       int           `json:"rating_count"`
	RelatedIds        []int         `json:"related_ids"`
	UpsellIds         []interface{} `json:"upsell_ids"`
	CrossSellIds      []interface{} `json:"cross_sell_ids"`
	ParentID          int           `json:"parent_id"`
	PurchaseNote      string        `json:"purchase_note"`
	Categories        []Categories  `json:"categories"`
	Tags              []interface{} `json:"tags"`
	Images            []Images      `json:"images"`
	Attributes        []interface{} `json:"attributes"`
	DefaultAttributes []interface{} `json:"default_attributes"`
	Variations        []interface{} `json:"variations"`
	GroupedProducts   []interface{} `json:"grouped_products"`
	MenuOrder         int           `json:"menu_order"`
	MetaData          []MetaData    `json:"meta_data"`
	Links             ProductLinks  `json:"_links"`
}

type ProductLinks struct {
	Self []struct {
		Href string `json:"href"`
	} `json:"self"`
	Collection []struct {
		Href string `json:"href"`
	} `json:"collection"`
}
type MetaData struct {
	ID    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"-"`
}
type Images struct {
	ID              int    `json:"id"`
	DateCreated     string `json:"date_created"`
	DateCreatedGmt  string `json:"date_created_gmt"`
	DateModified    string `json:"date_modified"`
	DateModifiedGmt string `json:"date_modified_gmt"`
	Src             string `json:"src"`
	Name            string `json:"name"`
	Alt             string `json:"alt"`
	Position        int    `json:"position"`
}
type Categories struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Slug string `json:"slug"`
}
type Dimensions struct {
	Length string `json:"length"`
	Width  string `json:"width"`
	Height string `json:"height"`
}

type ProductCategories struct {
	Categories []ProductCategory
}

type ProductCategory struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Slug        string `json:"slug"`
	Parent      int    `json:"parent"`
	Description string `json:"description"`
	Display     string `json:"display"`
	Image       string `json:"image"`
	Count       int    `json:"count"`

	Checked bool `json:"-"`
}
type OrderNote struct {
	ID             int    `json:"id"`
	Author         string `json:"author"`
	DateCreated    string `json:"date_created"`
	DateCreatedGmt string `json:"date_created_gmt"`
	Note           string `json:"note"`
	CustomerNote   bool   `json:"customer_note"`
	Links          struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
		Up []struct {
			Href string `json:"href"`
		} `json:"up"`
	} `json:"_links"`
}

type ProductsStatus struct {
	ID     int    `json:"id"`
	Status string `json:"status"`
}

type T int

func (o *Order) ArrayValues() []string {
	var data []string
	for _, l := range o.LineItems {
		for _, j := range l.MetaData {
			data = strings.Split(j.Value, ",")
		}
	}
	return data
}
