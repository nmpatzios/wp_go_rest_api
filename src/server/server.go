package main

import (
	"boltdb"
	"edesia"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"models"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	"woocommerce"
	wc "woocommerce"

	"github.com/gorilla/mux"

	// "github.com/shirou/gopsutil/host"
	_ "net/http/pprof"

	logurus "github.com/sirupsen/logrus"
)

var initialized bool
var c models.Conf

var templateCache = map[string]*template.Template{}
var customerKeysecret boltdb.CustomerKeysecret

var templateFuncMap = template.FuncMap{
	"safehtml":      safeHTML,
	"add":           add,
	"date":          date,
	"parsefloat":    parsefloat,
	"arrays":        arrays,
	"trim":          trim,
	"smscalculator": smscalculator,
}
var stop = false
var client *wc.Client

type LogFormat struct {
	TimestampFormat string
}

func smscalculator(price, unit string) string {
	priceint, _ := strconv.Atoi(price)
	unitint, _ := strconv.Atoi(unit)
	totalsms := (priceint * 100) / unitint

	return strconv.Itoa((totalsms))

}

func arrays(a string) []string {

	return strings.Split(a, ",")
}

var initTemplate = template.Must(template.New("4041.html").Funcs(templateFuncMap).ParseFiles("views/4041.html"))

func init() {
	c.GetConf()

	CreateDirIfNotExist("orders")

	// logurus.SetFormatter(&logurus.TextFormatter{FullTimestamp: true, TimestampFormat: "2006-01-02 15:04:05"})
	logurus.SetReportCaller(true)

	// log.SetOutput(os.Stdout)

	// logurus.SetFormatter(&logurus.TextFormatter{FullTimestamp: true})
	// logurus.SetReportCaller(true)
	logurus.SetFormatter(&logurus.JSONFormatter{})

	var filename string = "logfile.log"
	// Create the log file if doesn't exist. And append to it if it already exists.
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)

	if err != nil {
		// Cannot open log file. Logging to stderr
		logurus.Error(err)
	}
	ws := io.MultiWriter(os.Stdout, f)
	logurus.SetOutput(ws)

}
func doEvery(d time.Duration, f func(time.Time)) {

	for x := range time.Tick(d) {
		f(x)
	}
}

func orders(t time.Time) {
	fmt.Printf("%v:\n", t.Format("2006-01-02 15:04:05"))
	var msg string
	data, err := client.GetProcessingOrderNewApproach(Limit, DefaultForm)
	if err != nil {
		logurus.Errorf(err.Error())
		msg = err.Error()
	}

	if err == nil && len(data) == 0 {

		msg = "<center> <img src='/static/images/checklist.png'> <h3>Δεν υπάρχουν νεες παραγγελίες.</h3></center>"

	}
	if len(data) > 0 {
		msg, err = woocommerce.RenderOrderTemplateString(woocommerce.OrdersTemplateHTML, data)
		if err != nil {
			logurus.Errorf(err.Error())
			msg = err.Error()

		}
	}

	logch <- msg
}
func CacheMiddleware(h http.Handler, duration time.Duration) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", fmt.Sprintf("public, max-age=%d", int(duration.Seconds())))
		w.Header().Set("Expires", time.Now().Add(duration).Format(http.TimeFormat))
		h.ServeHTTP(w, r)
	})
}

func renderView(viewName string, w http.ResponseWriter, data interface{}) {
	if tmpl, ok := templateCache[viewName]; ok {
		tmpl.Execute(w, data)
		return
	}
	tmpl, err := template.New("base.html").Funcs(templateFuncMap).ParseFiles("views/base.html", "views/"+viewName)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"templateCache[viewName]": templateCache[viewName],
			"viewName":                viewName,
			"error":                   err.Error(),
		}).Fatalln("Error parsing template")
	}
	templateCache[viewName] = tmpl
	err = tmpl.Execute(w, data)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"templateCache[viewName]": templateCache[viewName],
			"viewName":                viewName,
			"error":                   err.Error(),
		}).Fatalln("Error executing template")
	}
}

func safeHTML(html string) template.HTML {
	return template.HTML(html)
}
func trim(a string) string {
	if strings.ContainsAny(a, "&") {
		return strings.TrimSpace("")
	}

	if strings.TrimSpace(a) == "yes" {
		return "\t"
	}

	if _, err := strconv.Atoi(strings.TrimSpace(a)); err == nil {
		return "\t"
	}

	return fmt.Sprintf("- %s", strings.TrimSpace(a))
}

func date(date string) string {
	yourDate, _ := time.Parse("2006-01-02T15:04:05", date)
	return yourDate.Format("02-1-2006 15:04")
}
func add(x, y int) int {
	return x + y
}
func parsefloat(text string) string {
	number, _ := strconv.ParseFloat(text, 64)
	return fmt.Sprintf("%.2f", float64(number))

}
func task() {
	fmt.Println("I am runnning task.")

}

func ProcessingOrders(w http.ResponseWriter, r *http.Request) {

	context := GetOrders()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	// context.Orders = client.GetOrder()
	context.User = &user1
	context.Host = r.Host

	// <-ticker.C
	t := time.Now()
	go orders(t)

	renderView("processing-orders.html", w, context)

}

func CompletedOrders(w http.ResponseWriter, r *http.Request) {

	pageNumberStr := r.URL.Query().Get("page")
	pageNumber, _ := strconv.Atoi(pageNumberStr)
	if pageNumber == 0 {
		pageNumber = 1
	}

	context := GetCompletedOrders()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	// context.Orders = client.ConcurrentCompletedOrders("completed")
	context.User = &user1
	context.Host = r.Host

	// <-ticker.C
	// if stop == true {
	// 	quit <- struct{}{}
	// }
	// stop = true

	var orders []wc.Order
	b, err := json.MarshalIndent(client.GetCompletedOrdersOrder("completed", pageNumber), " ", " ")
	if err != nil {
		logurus.Error(err.Error())
	}
	// println(string(b))
	json.Unmarshal(b, &orders)

	context.Orders = append(context.Orders, orders...)

	if len(context.Orders) > 0 {
		for i, o := range context.Orders {
			if len(o.ShippingLines) > 0 {
				context.Orders[i].ShippingMethod = o.ShippingLines[0].MethodTitle

			}

			for j := range o.LineItems {

				for k := range o.LineItems[j].MetaData {
					if o.LineItems[j].MetaData[k].Key == "_free_gift" || o.LineItems[j].MetaData[k].Key == "_rule_id_free_gift" {
						context.Orders[i].LineItems[j].MetaData[k] = wc.LineItemMetaData{}

					}
				}

			}

		}

	}

	context.TotalPages = client.TotalPages()
	context.TotalProducts = client.Total()

	renderView("ordertypes.html", w, context)

}

func CancelledOrders(w http.ResponseWriter, r *http.Request) {
	pageNumberStr := r.URL.Query().Get("page")
	pageNumber, _ := strconv.Atoi(pageNumberStr)
	if pageNumber == 0 {
		pageNumber = 1
	}

	context := GetCancelledOrders()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	// context.Orders = client.ConcurrentCompletedOrders("cancelled")
	context.User = &user1
	// <-ticker.C
	// if stop == true {
	// 	quit <- struct{}{}
	// }
	// stop = true
	context.Host = r.Host

	var orders []wc.Order
	b, err := json.MarshalIndent(client.GetCompletedOrdersOrder("cancelled", pageNumber), " ", " ")
	if err != nil {
		logurus.Error(err.Error())
	}
	json.Unmarshal(b, &orders)

	context.Orders = append(context.Orders, orders...)
	if len(context.Orders) > 0 {
		for i, o := range context.Orders {
			if len(o.ShippingLines) > 0 {
				context.Orders[i].ShippingMethod = o.ShippingLines[0].MethodTitle

			}
			for j := range o.LineItems {

				for k := range o.LineItems[j].MetaData {
					if o.LineItems[j].MetaData[k].Key == "_free_gift" || o.LineItems[j].MetaData[k].Key == "_rule_id_free_gift" {
						context.Orders[i].LineItems[j].MetaData[k] = wc.LineItemMetaData{}

					}
				}

			}

		}
	}

	context.TotalPages = client.TotalPages()
	context.TotalProducts = client.Total()

	renderView("ordertypes.html", w, context)

}
func search(w http.ResponseWriter, r *http.Request) {
	search := r.FormValue("search")

	println(search)

	context := GetCustomers()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}

	var customers []woocommerce.WPCustomer
	b, err := json.Marshal(client.Search("customers", search))
	if err != nil {
		logurus.Error(err.Error())
	}
	json.Unmarshal(b, &customers)

	context.Customers = append(context.Customers, customers...)

	context.User = &user1

	context.Host = r.Host
	context.TotalPages = client.TotalPages()
	context.TotalProducts = client.Total()
	context.Search = search
	context.Title = fmt.Sprintf("Αποτελέσματα αναζήτης για '%s' ", search)

	renderView("customer-search.html", w, context)

}

func ordersPerCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		println(err.Error())
	}

	name := vars["customername"]

	println(name)

	context := GetCustomers()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}

	var orders []woocommerce.OrdersPerCustomer
	b, err := json.Marshal(client.OrdersPerCustomer("orders", ID))
	if err != nil {
		logurus.Error(err.Error())
	}
	json.Unmarshal(b, &orders)

	var completedOrders []int
	var rest []int
	var money float64

	for i, order := range orders {

		if s, err := strconv.ParseFloat(order.Total, 64); err == nil {
			money = money + s
		}
		if order.Status == "completed" {
			completedOrders = append(completedOrders, order.ID)
		} else {
			rest = append(rest, order.ID)
		}
		for j := range order.LineItems {

			for k := range order.LineItems[j].MetaData {
				if order.LineItems[j].MetaData[k].Key == "_free_gift" || order.LineItems[j].MetaData[k].Key == "_rule_id_free_gift" {
					orders[i].LineItems[j].MetaData[k] = wc.LineItemMetaData{}

				}
			}

		}
	}

	context.OrdersPerCustomer = append(context.OrdersPerCustomer, orders...)

	context.User = &user1

	context.Host = r.Host
	context.TotalPages = client.TotalPages()
	context.TotalProducts = client.Total()
	context.Title = name
	context.TotalOdersPerCustomer = len(completedOrders)
	context.TotalCancelOdersPerCustomer = len(rest)
	context.TotalOdersPerCustomerMoney = money

	renderView("customer-orders.html", w, context)

}

func renderCustomers(w http.ResponseWriter, r *http.Request) {
	context := GetCustomers()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}

	pageNumberStr := r.URL.Query().Get("page")
	pageNumber, _ := strconv.Atoi(pageNumberStr)
	if pageNumber == 0 {
		pageNumber = 1
	}

	// for {
	var customers []woocommerce.WPCustomer
	jsonMarshall, err := client.Getwoos("customers", pageNumber, false)
	if err != nil {
		logurus.Error(err.Error())
	}
	b, err := json.Marshal(jsonMarshall)
	if err != nil {
		logurus.Error(err.Error())
	}
	json.Unmarshal(b, &customers)

	context.Customers = append(context.Customers, customers...)
	// if len(customers) < 90 {
	// 	break
	// }
	// counter++
	// }

	context.User = &user1
	// fmt.Println(client.Link())
	// fmt.Println(client.Total())
	// fmt.Println(client.TotalPages())
	// if stop == true {
	// 	quit <- struct{}{}
	// }
	// stop = true
	context.Host = r.Host
	context.TotalPages = client.TotalPages()
	context.TotalProducts = client.Total()

	renderView("contacts.html", w, context)

}

func renderProducts(w http.ResponseWriter, r *http.Request) {
	context := GetProducts()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	context.User = &user1
	// if stop == true {
	// 	quit <- struct{}{}
	// }
	// stop = true

	// context.Products = client.GetProducts()
	pageNumberStr := r.URL.Query().Get("page")
	pageNumber, _ := strconv.Atoi(pageNumberStr)
	if pageNumber == 0 {
		pageNumber = 1
	}
	context.Host = r.Host

	counter := 1

	for {
		var categories []*woocommerce.ProductCategory
		jsonMarshall, err := client.Getwoos("products/categories", counter, true)
		if err != nil {
			logurus.Error(err.Error())
		}
		b, err := json.Marshal(jsonMarshall)

		if err != nil {
			logurus.Error(err.Error())
		}
		json.Unmarshal(b, &categories)

		context.Categories = append(context.Categories, categories...)
		if len(categories) < 90 {
			break
		}
		counter++
	}

	renderView("products.html", w, context)

}

func PackagesPointer(products []wc.Product) *wc.Products {
	return &wc.Products{
		Products: products,
	}
}
func renderPricingTable(w http.ResponseWriter, r *http.Request) {
	context := GetPricingTablePage()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	p, err := edesia.GetProducts()
	if err != nil {
		println(err.Error())
	}

	for i := range p {
		p[i].PackageFeatures = strings.Split(strings.TrimSpace(strings.Replace(strings.Replace(p[i].Description, "<p>", "", -1), "</p>", "", -1)), ",")
	}
	models.PackagesProducts = PackagesPointer((p))
	context.Products = p
	context.User = &user1
	context.Host = r.Host

	// /<p>(.*?)<\/p>/
	// reg, _ := regexp.Compile("<p>(.*?)</p>")

	renderView("pricing-table.html", w, context)

}

func renderCustomerDetails(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		println(err.Error())
	}
	// name := vars["name"]

	context := GetCustomerForm()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}

	for _, p := range models.PackagesProducts.Products {
		if p.ID == ID {
			context.Product = p
			context.Title = p.Name
			break
		}
	}
	context.User = &user1
	context.Host = r.Host
	var customer woocommerce.WPCustomer
	jsonMarshall := client.SingleCustomer(user1.User.ID)

	b, err := json.Marshal(jsonMarshall)

	if err != nil {
		logurus.Error(err.Error())
	}
	json.Unmarshal(b, &customer)
	context.Customer = customer

	// /<p>(.*?)<\/p>/
	// reg, _ := regexp.Compile("<p>(.*?)</p>")

	renderView("customer-form.html", w, context)

}

func renderSettings(w http.ResponseWriter, r *http.Request) {
	context := GetSettingsPage()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	context.User = &user1
	// if stop == true {
	// 	quit <- struct{}{}
	// }
	// stop = true
	context.Host = r.Host

	renderView("settings.html", w, context)

}

func renderPrinterSettings(w http.ResponseWriter, r *http.Request) {
	context := GetPrinterSettingsPage()
	cookie := getUserName(r)

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	context.User = &user1
	// if stop == true {
	// 	quit <- struct{}{}
	// }
	// stop = true
	context.Host = r.Host

	renderView("printer-settings.html", w, context)

}

func renderProductsPost(w http.ResponseWriter, r *http.Request) {
	context := GetProducts()
	cookie := getUserName(r)

	r.ParseForm()

	categoriesSelected := r.Form["category"]
	selectall := r.FormValue("selectall")

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}
	context.User = &user1
	context.Host = r.Host

	// if stop == true {
	// 	quit <- struct{}{}
	// }
	// stop = true

	counter1 := 1

	for {
		var products []woocommerce.Product

		b, err := json.Marshal(client.GetProductsFlat(strings.Join(categoriesSelected, ","), "products", counter1))
		if err != nil {
			logurus.Error(err.Error())
		}
		json.Unmarshal(b, &products)

		context.Products = append(context.Products, products...)
		if len(products) < 90 {
			break
		}
		counter1++
	}
	pageNumberStr := r.URL.Query().Get("page")
	pageNumber, _ := strconv.Atoi(pageNumberStr)
	if pageNumber == 0 {
		pageNumber = 1
	}
	counter := 1

	for {
		var categories []*woocommerce.ProductCategory
		jsonMarshall, err := client.Getwoos("products/categories", counter, true)
		if err != nil {
			logurus.Error(err.Error())
		}
		b, err := json.Marshal(jsonMarshall)
		if err != nil {
			logurus.Error(err.Error())
		}
		json.Unmarshal(b, &categories)

		context.Categories = append(context.Categories, categories...)
		if len(categories) < 90 {
			break
		}
		counter++
	}

	for i := range context.Categories {
		for j := range categoriesSelected {
			intid, _ := strconv.Atoi(categoriesSelected[j])
			if context.Categories[i].ID == intid {
				context.Categories[i].Checked = true
			}
		}
	}

	// context.Categories = categories
	context.SelectedCategories = categoriesSelected

	if selectall != "" {
		context.SelectAll = true
	}
	// context.Products = client.ConcurrentProducts(strings.Join(categoriesSelected, ","))

	renderView("products.html", w, context)

}

func notFound(w http.ResponseWriter, r *http.Request) {

	context := Get404()

	username := getUserName(r)
	cookie := getUserName(r)

	var user1 models.WPLoginUser

	if cookie == "" {
		http.Redirect(w, r, "/admin/login", 302)
		return

	}

	context.User = &user1

	context.Username = username
	if len(username) != 0 {
		renderView("404.html", w, context)
		return
	} else if len(username) < 1 {
		var loginTemplate = template.Must(template.New("4041.html").Funcs(templateFuncMap).ParseFiles("views/4041.html"))
		err := loginTemplate.Execute(w, context)

		if err != nil {
			logurus.WithFields(logurus.Fields{
				"loginTemplate": loginTemplate,
				"error":         err.Error(),
			}).Fatalln("Error executing template ")
		}
	}

}

func main() {

	// fmt.Println(models.EncryptPass(models.KeyGenerator, "33411"))

	// create the logger
	// logger := logurus.New()
	// // with Json Formatter
	// logger.Formatter = &logurus.JSONFormatter{}
	// logger.SetOutput(os.Stdout)

	// file, err := os.OpenFile("log.txt", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0755)
	// if err != nil {
	// 	logger.Fatal(err)
	// }
	// defer file.Close()
	// logger.SetOutput(file)

	http.Handle("/static/", CacheMiddleware(http.StripPrefix("/static/", http.FileServer(http.Dir("static"))), 7*24*time.Hour))

	router := mux.NewRouter()
	router.HandleFunc("/", Index)
	router.Handle("/admin", AdminCheck(http.HandlerFunc(Admin), true))
	router.HandleFunc("/admin/login", login).Methods("GET")
	router.HandleFunc("/admin/login", loginPost).Methods("POST")
	router.Handle("/admin/logout", AdminCheck(http.HandlerFunc(logout), true))
	router.Handle("/admin/processing-orders", AdminCheck(http.HandlerFunc(ProcessingOrders), true))
	router.Handle("/admin/completed-orders", AdminCheck(http.HandlerFunc(CompletedOrders), true))
	router.Handle("/admin/cancelled-orders", AdminCheck(http.HandlerFunc(CancelledOrders), true))

	router.Handle("/admin/customers", AdminCheck(http.HandlerFunc(renderCustomers), true))
	router.Handle("/admin/products", AdminCheck(http.HandlerFunc(renderProducts), true)).Methods("GET")
	router.Handle("/admin/products", AdminCheck(http.HandlerFunc(renderProductsPost), true)).Methods("POST")

	router.Handle("/admin/pricing", AdminCheck(http.HandlerFunc(renderPricingTable), true)).Methods("GET")
	router.Handle("/admin/pricing/package/{id}/{name}", AdminCheck(http.HandlerFunc(renderCustomerDetails), true)).Methods("GET")

	router.Handle("/admin/settings", AdminCheck(http.HandlerFunc(renderSettings), true)).Methods("GET")
	router.Handle("/admin/printer-settings", AdminCheck(http.HandlerFunc(renderPrinterSettings), true)).Methods("GET")

	router.Handle("/admin/api/change-order-status", AdminCheck(http.HandlerFunc(UpdateStatusJSON), true))

	router.Handle("/admin/api/update-products", AdminCheck(http.HandlerFunc(UpdateProducts), true))
	router.Handle("/admin/api/tiposi/{id}", AdminCheck(http.HandlerFunc(Tiposi), true))

	router.Handle("/admin/api/insert_customer_key_secret", AdminCheck(http.HandlerFunc(InsertCustomerKeySecret), true))
	router.Handle("/admin/api/total-characters/{id}", AdminCheck(http.HandlerFunc(TotalCharacters), true))

	router.Handle("/admin/api/format-order", AdminCheck(http.HandlerFunc(FormatOrder), true))

	router.Handle("/admin/api/create-order-note", AdminCheck(http.HandlerFunc(CreateOrderNoteJSON), true))

	router.Handle("/admin/api/set-to-default-form", AdminCheck(http.HandlerFunc(SettoDefaultForm), true))
	router.Handle("/admin/api/totals", AdminCheck(http.HandlerFunc(Totals), true))

	router.Handle("/admin/api/check-if-admin/{adminpass}", AdminCheck(http.HandlerFunc(CheckIfAdmin), true))

	router.Handle("/admin/orders/ws", AdminCheck(http.HandlerFunc(GetWebsocket), true))

	router.Handle("/admin/api/all-sales/{from}/{to}", AdminCheck(http.HandlerFunc(GetSales), true))

	router.Handle("/admin/search-results", AdminCheck(http.HandlerFunc(search), true)).Methods("POST")

	router.Handle("/admin/customers/{id}/{customername}", AdminCheck(http.HandlerFunc(ordersPerCustomer), true)).Methods("GET")

	router.Handle("/admin/api/buy-credits", AdminCheck(http.HandlerFunc(BuyCredits), true))

	// router.Handle("/admin/products/ws", AdminCheck(http.HandlerFunc(wc.GetWebsocket), true))
	// router.Handle("/admin/orderstatuses/ws", AdminCheck(http.HandlerFunc(wc.GetWebsocket), true))

	router.NotFoundHandler = http.HandlerFunc(notFound)
	http.Handle("/", router)
	log.Printf("Listening on port %+v\n", c.HTTPPLATFORMPORT)

	log.Println(http.ListenAndServe(":"+c.HTTPPLATFORMPORT, nil))
	// http.ListenAndSerrve(":3001", nil)

}
