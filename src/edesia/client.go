package edesia

import (
	"bytes"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"models"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"
)

var conf models.Conf

const (
	Version       = "1.0.0"
	UserAgent     = "WooCommerce GO API VERSION/" + Version
	HashAlgorithm = "HMAC-SHA256"
)

type Options struct {
	API             bool
	APIPrefix       string
	Version         string
	Timeout         time.Duration
	VerifySSL       bool
	QueryStringAuth string
	OauthTimestamp  time.Time
}

type Client struct {
	StoreURL  *url.URL
	Ck        string
	Cs        string
	Option    *Options
	RawClient *http.Client
}

func GetJson(url string, target interface{}) error {
	r, err := http.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func NewClient(store, ck, cs string, option *Options) (*Client, error) {

	storeURL, err := url.Parse(store)
	if err != nil {
		return nil, err
	}

	if option == nil {
		option = &Options{}
	}
	if option.OauthTimestamp.IsZero() {
		option.OauthTimestamp = time.Now()
	}

	if option.Version == "" {
		option.Version = "v3"
	}
	path := "/wp-json/wc/"
	if option.API {
		path = option.APIPrefix
	}
	path = path + option.Version + "/"

	storeURL.Path = path

	rawClient := &http.Client{}
	rawClient.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: option.VerifySSL},
	}

	return &Client{
		StoreURL:  storeURL,
		Ck:        ck,
		Cs:        cs,
		Option:    option,
		RawClient: rawClient,
	}, nil
}

func GetClient() (*Client, error) {
	c := conf.GetConf()
	client, err := NewClient(
		c.EDESIADOMAIN,
		c.EDESIAKEY,
		c.EDESIASECRET,
		&Options{
			API:     true,
			Version: "/wp-json/wc/v3",
		},
	)

	if err != nil {
		return nil, err
	}
	return client, nil
}

// func GetClient() (*edesia.Client, error) {
// 	client, err := edesia.NewClient(
// 		"https://edesia.gr",
// 		"ck_dcece3e28ce3ca76e4e6121561fadc4bdbcca138",
// 		"cs_a9e88fa26f7c7121f9171d0122e8f67caf0cbcd6",
// 		&edesia.Options{
// 			API:     true,
// 			Version: "/wp-json/wc/v3",
// 		},
// 	)

// 	if err != nil {
// 		return nil, err
// 	}
// 	return client, nil
// }
func (c *Client) basicAuth(params url.Values) string {
	params.Add("consumer_key", c.Ck)
	params.Add("consumer_secret", c.Cs)
	return params.Encode()
}

func (c *Client) oauth(method, urlStr string, params url.Values) string {
	params.Add("oauth_consumer_key", c.Ck)
	params.Add("oauth_timestamp", strconv.Itoa(int(c.Option.OauthTimestamp.Unix())))
	nonce := make([]byte, 16)
	rand.Read(nonce)
	sha1Nonce := fmt.Sprintf("%x", sha1.Sum(nonce))
	params.Add("oauth_nonce", sha1Nonce)
	params.Add("oauth_signature_method", HashAlgorithm)
	var keys []string
	for k, _ := range params {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var paramStrs []string
	for _, key := range keys {
		paramStrs = append(paramStrs, fmt.Sprintf("%s=%s", key, params.Get(key)))
	}
	paramStr := strings.Join(paramStrs, "&")
	params.Add("oauth_signature", c.oauthSign(method, urlStr, paramStr))
	return params.Encode()
}

func (c *Client) oauthSign(method, endpoint, params string) string {
	signingKey := c.Cs
	if c.Option.Version != "v1" || c.Option.Version != "v2" {
		signingKey = signingKey + "&"
	}

	a := strings.Join([]string{method, url.QueryEscape(endpoint), url.QueryEscape(params)}, "&")
	mac := hmac.New(sha256.New, []byte(signingKey))
	mac.Write([]byte(a))
	signatureBytes := mac.Sum(nil)
	return base64.StdEncoding.EncodeToString(signatureBytes)
}

func (c *Client) request(method, endpoint string, params url.Values, data interface{}) (io.ReadCloser, error) {
	urlstr := c.StoreURL.String() + endpoint
	if params == nil {
		params = make(url.Values)
	}
	if c.StoreURL.Scheme == "https" {
		urlstr += "?" + c.basicAuth(params)
	} else {
		urlstr += "?" + c.oauth(method, urlstr, params)
	}
	switch method {
	case http.MethodPost, http.MethodPut:
	case http.MethodDelete, http.MethodGet, http.MethodOptions:
	default:
		return nil, fmt.Errorf("Method is not recognised: %s", method)
	}

	body := new(bytes.Buffer)
	encoder := json.NewEncoder(body)
	if err := encoder.Encode(data); err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, urlstr, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := c.RawClient.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == http.StatusBadRequest ||
		resp.StatusCode == http.StatusUnauthorized ||
		resp.StatusCode == http.StatusNotFound ||
		resp.StatusCode == http.StatusInternalServerError {
		return nil, fmt.Errorf("Request failed: %s", resp.Status)
	}
	return resp.Body, nil
}

func (c *Client) Post(endpoint string, data interface{}) (io.ReadCloser, error) {
	return c.request("POST", endpoint, nil, data)
}

func (c *Client) Put(endpoint string, data interface{}) (io.ReadCloser, error) {
	return c.request("PUT", endpoint, nil, data)
}

func (c *Client) Get(endpoint string, params url.Values) (io.ReadCloser, error) {
	return c.request("GET", endpoint, params, nil)
}
