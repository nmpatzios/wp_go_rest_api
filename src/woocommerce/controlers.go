package woocommerce

import (
	"boltdb"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"printers"
	"sort"
	"strconv"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
	logurus "github.com/sirupsen/logrus"
)

func GetJson(url string, target interface{}) error {
	// println(url)
	r, err := http.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

var proch chan string

var proccess struct {
	Orders []Order
}

var lastorderID int

func (c *Client) GetProcessingOrderNewApproach(limit int, defaultForm string) ([]Order, error) {

	_, err := net.LookupHost("google.com")
	if err != nil {

		return []Order{}, errors.New("<center> <img src='/static/images/errornet.png'> <h3>Δεν υπάρχει σύνδεση στο δίκτυο.</h3></center>")
	}

	// v := url.Values{}

	var data []Order
	var lenorders []Order
	req, err := http.NewRequest("GET", fmt.Sprintf("%sorders", c.storeURL), nil)
	if err != nil {
		return []Order{}, err
	}

	q := req.URL.Query()

	q.Add("per_page", "99")
	q.Add("status", "processing")
	q.Add("consumer_key", c.ck)
	q.Add("consumer_secret", c.cs)
	req.URL.RawQuery = q.Encode()

	err = GetJson(req.URL.String(), &data)
	if err != nil {
		return []Order{}, err

	}

	db, err := boltdb.SetupDB()
	if err != nil {
		defer db.Close()
		return []Order{}, err

	}
	defer db.Close()

	GetLastOrderID, err := boltdb.GetLastOrderID(db, lastorderID)

	// println(GetLastOrderID)

	// inrec, _ := json.Marshal(data)
	if len(data) > 0 {
		sort.Slice(data, func(i, j int) bool {
			return data[i].ID < data[j].ID
		})

		for i := range data {

			for _, m := range data[i].MetaData {
				if m.Key == "_billing_lat" {
					data[i].Lat = m.Value
				}
				if m.Key == "_billing_lng" {
					data[i].Lon = m.Value
				}
			}

			// fmt.Printf("%+v\n", data[i].MetaData)
			data[i].ShippingMethod = data[i].ShippingLines[0].MethodTitle
			for j := range data[i].LineItems {

				for k := range data[i].LineItems[j].MetaData {
					if data[i].LineItems[j].MetaData[k].Key == "_free_gift" || data[i].LineItems[j].MetaData[k].Key == "_rule_id_free_gift" {
						data[i].LineItems[j].MetaData[k] = LineItemMetaData{}

					}
				}

			}

			if data[i].ID > GetLastOrderID {
				lenorders = append(lenorders, data[i])
				boltdb.SetLastOrderID(db, data[len(data)-1].ID)

			}
		}

		if len(lenorders) > 0 {
			inrec, _ := json.Marshal(lenorders)

			err := printers.SendToPrinter(inrec, limit, defaultForm)
			if err != nil {
				return data, err
			}
			go playMusic()

		}
	}

	return data, nil
}

func playMusic() {
	f, err := os.Open("static/images/dekas.mp3")
	if err != nil {
		log.Fatal(err)
	}

	streamer, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatal(err)
	}
	defer streamer.Close()

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	done := make(chan bool)
	speaker.Play(beep.Seq(streamer, beep.Callback(func() {
		done <- true
	})))

	<-done
}

func difference(slice1 []Order, slice2 []Order) []Order {
	var diff []Order

	// Loop two times, first to find slice1 strings not in slice2,
	// second loop to find slice2 strings not in slice1
	for i := 0; i < 2; i++ {
		for _, s1 := range slice1 {
			found := false
			for _, s2 := range slice2 {
				if s1.ID == s2.ID {
					found = true
					break
				}
			}
			// String not found. We add it to return slice
			if !found {
				diff = append(diff, s1)
			}
		}
		// Swap the slices, only if it was the first loop
		if i == 0 {
			slice1, slice2 = slice2, slice1
		}
	}

	return diff
}
func (c *Client) GetCompletedOrdersOrder(status string, counter int) interface{} {

	// logurus.Info("refresh")
	v := url.Values{}

	// var data []Order
	// var newdata []Order

	// for page := 1; ; page++ {

	// v.Add("per_page", "10")
	v.Add("status", status)
	v.Add("page", strconv.Itoa(counter))

	body, err := c.Get("orders", v)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting precessing orders")
	}

	jsonDecoder := json.NewDecoder(body)

	var data []map[string]interface{}
	if err := jsonDecoder.Decode(&data); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting precessing orders")
	}
	defer body.Close()

	// fmt.Println(data)

	// }

	return data

}

func (c *Client) ChangeProductStatus(products []ProductsStatus) {

	start := time.Now()
	numComplete := 0
	var data []ProductsStatus

	for _, p := range products {
		go func(p ProductsStatus) {
			mapD := map[string]string{"status": p.Status}

			body, err := c.Post("products/"+strconv.Itoa(p.ID), mapD)
			if err != nil {
				logurus.WithFields(logurus.Fields{
					"body":  body,
					"error": err.Error(),
				}).Errorln("Error updating product with id:" + strconv.Itoa(p.ID))
			}
			jsonDecoder := json.NewDecoder(body)
			if err := jsonDecoder.Decode(&p); err != nil && err != io.EOF {
				logurus.WithFields(logurus.Fields{
					"jsonDecoder": jsonDecoder,
					"error":       err.Error(),
				}).Errorln("Error decoding  json")
			}

			data = append(data, p)

			defer body.Close()

			numComplete++
			// logurus.Info("Successfully updated product with id:  " + strconv.Itoa(p.ID))
		}(p)
	}

	for numComplete < len(products) {
		time.Sleep(50 * time.Millisecond)
	}
	elapsed := time.Since(start)

	if proch != nil {
		logurus.Errorf("cannot change product with id: %s ", strconv.Itoa(products[numComplete].ID))
	}

	fmt.Printf("Execution Time: %s", elapsed)
}
func (c *Client) GetProductsFlat(categoryIds, query string, counter int) interface{} {
	start := time.Now()
	v := url.Values{}

	v.Add("per_page", "90")

	v.Add("page", strconv.Itoa(counter))
	v.Add("category", categoryIds)

	body, err := c.Get(query, v)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error getting products")
	}

	jsonDecoder := json.NewDecoder(body)
	var data []map[string]interface{}
	if err := jsonDecoder.Decode(&data); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"jsonDecoder": jsonDecoder,
			"error":       err.Error(),
		}).Errorln("Error decoding  json")
	}
	defer body.Close()

	elapsed := time.Since(start)

	fmt.Printf("Execution Time: %s", elapsed)

	return data
}

func (c *Client) ChangeOrderStatus(status string, id int) (Order, error) {

	// for i, b := range lenorders {
	// 	if b.ID == id {
	// 		lenorders = append(lenorders[:i], lenorders[i+1:]...)
	// 	}
	// }

	var order Order
	mapD := map[string]string{"status": status}
	// data, _ := json.Marshal(mapD)
	// println(string(data))
	println(id, status)

	body, err := c.Put("orders/"+strconv.Itoa(id), mapD)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error getting products")
		return Order{}, err
	}

	jsonDecoder := json.NewDecoder(body)
	if err := jsonDecoder.Decode(&order); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"jsonDecoder": jsonDecoder,
			"error":       err.Error(),
		}).Errorln("Error decoding  json")
		return Order{}, err
	}
	defer body.Close()

	return order, nil

}

func (c *Client) CreateOrderNote(message string, id string) (OrderNote, error) {

	var note OrderNote

	mapD := map[string]string{"note": message, "customer_note": "true"}
	data, _ := json.Marshal(mapD)
	println(string(data))

	body, err := c.Post("orders/"+id+"/notes", mapD)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error updating order note")
		return OrderNote{}, err
	}

	jsonDecoder := json.NewDecoder(body)
	if err := jsonDecoder.Decode(&note); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"jsonDecoder": jsonDecoder,
			"error":       err.Error(),
		}).Errorln("Error decoding  json")
		return OrderNote{}, err
	}
	defer body.Close()

	// logurus.Info(note.Note)

	return note, nil

}
