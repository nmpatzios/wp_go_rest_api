package woocommerce

import (
	"encoding/json"
	"io"
	"net/url"
	"strconv"

	logurus "github.com/sirupsen/logrus"
)

func (c *Client) Getwoos(queryurl string, pagegNumber int, isCategory bool) (interface{}, error) {

	// logurus.Info("refresh")
	v := url.Values{}
	var data []map[string]interface{}

	// var data []Order
	// var newdata []Order

	// for page := 1; ; page++ {
	if isCategory == true {
		v.Add("per_page", "90")
	}

	// v.Add("per_page", "40")

	v.Add("page", strconv.Itoa(pagegNumber))
	body, err := c.Get(queryurl, v)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting precessing orders")
		return data, err
	}

	jsonDecoder := json.NewDecoder(body)

	if err := jsonDecoder.Decode(&data); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting precessing orders")
		return data, err

	}
	defer body.Close()

	// fmt.Println(data)

	// }

	return data, nil

}

func (c *Client) Search(queryurl string, search string) interface{} {

	// logurus.Info("refresh")
	v := url.Values{}

	// var data []Order
	// var newdata []Order

	// for page := 1; ; page++ {

	v.Add("search", search)
	body, err := c.Get(queryurl, v)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting precessing orders")
	}

	jsonDecoder := json.NewDecoder(body)

	var data []map[string]interface{}
	if err := jsonDecoder.Decode(&data); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting precessing orders")
	}
	defer body.Close()

	// fmt.Println(data)

	// }

	return data

}

func (c *Client) OrdersPerCustomer(queryurl string, id int) interface{} {

	// logurus.Info("refresh")
	v := url.Values{}

	// var data []Order
	// var newdata []Order

	// for page := 1; ; page++ {

	v.Add("customer", strconv.Itoa(id))
	body, err := c.Get(queryurl, v)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error getting customer orders")
	}

	jsonDecoder := json.NewDecoder(body)

	var data []map[string]interface{}
	if err := jsonDecoder.Decode(&data); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting customer orders")
	}
	defer body.Close()

	// fmt.Println(data)

	// }

	return data

}

func (c *Client) SingleCustomer(id int) interface{} {

	body, err := c.Get("customers/"+strconv.Itoa(id), nil)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error getting customer orders")
	}

	jsonDecoder := json.NewDecoder(body)

	var data map[string]interface{}
	if err := jsonDecoder.Decode(&data); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting customer orders")
	}
	defer body.Close()

	return data

}
