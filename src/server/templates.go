package main

var templatesMap map[string]string

const printTmpl = `DELIEVRY - ΑΠΟ SITE
ΑΡΙΘΜΟΣ ΠΑΡΑΓΓΕΛΙΑΣ: {{.ID}}
ΠΕΛΑΤΗΣ:  {{.Billing.FirstName}} {{.Billing.LastName}}
ΔΙΕΥΘΥΝΣΗ: {{.Billing.Address1}}
ΠΟΛΗ: {{.Billing.City}}
ΤΗΛ:  {{.Billing.Phone}}
ΚΙΝΗΤΟ: {{.Billing.Phone}}
ΟΡΟΦΟΣ: {{.Billing.Address2}}
ΣΧΟΛΙΑ ΠΑΡΑΓΓΕΛΙΑΣ: {{.CustomerNote}}
ΗΜΕΡ.:{{.DateCreated}}
ΤΡΟΠΟΣ ΠΛΗΡΩΜΗΣ:  {{.PaymentMethodTitle}}
----------------------------------------
ΠΟΣΟΤ     ΕΙΔΟΣ		             	    ΑΞΙΑ
----------------------------------------
{{range .LineItems}}{{.Product}}{{range .MetaData}}{{ range  $value := arrays .Value }}
{{trim $value}}{{end}}{{end}}
{{end}}
----------------------------------------
ΕΚΠΤΩΣΗ ΚΟΥΠΟΝΙ              {{.DiscountTotal}} EURO

[{{ $length := len .LineItems }}{{$length}}]ΣΥΝΟΛΟ                      {{.Total}}  EURO



`
