package boltdb

import (
	"encoding/json"
	"fmt"

	"github.com/boltdb/bolt"
	logurus "github.com/sirupsen/logrus"
)

func SetTotalCharacters(db *bolt.DB, total int) error {
	confBytes, err := json.Marshal(total)
	if err != nil {
		return fmt.Errorf("could not marshal config json: %v", err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		err = tx.Bucket([]byte("DB")).Put([]byte("TOTALCHARACTERS"), confBytes)
		if err != nil {
			return fmt.Errorf("could not set config: %v", err)
		}
		return nil
	})
	return err
}

func GtTotalCharacters(db *bolt.DB, total int) (int, error) {
	err := db.View(func(tx *bolt.Tx) error {
		conf := tx.Bucket([]byte("DB")).Get([]byte("TOTALCHARACTERS"))
		// fmt.Printf("Config: %s\n", conf)

		err := json.Unmarshal(conf, &total)
		if err != nil {
			logurus.Error(err.Error())
			return err
		}
		return nil
	})
	if err != nil {
		logurus.Error(err.Error())
		return 0, err
	}

	return total, nil
}

func SetCusstomForm(db *bolt.DB, form string) error {
	confBytes, err := json.Marshal(form)
	if err != nil {
		return fmt.Errorf("could not marshal config json: %v", err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		err = tx.Bucket([]byte("DB")).Put([]byte("CUSTOMFORM"), confBytes)
		if err != nil {
			return fmt.Errorf("could not set config: %v", err)
		}
		return nil
	})
	return err
}

func GetCusstomForm(db *bolt.DB, form string) (string, error) {
	err := db.View(func(tx *bolt.Tx) error {
		conf := tx.Bucket([]byte("DB")).Get([]byte("CUSTOMFORM"))
		// fmt.Printf("Config: %s\n", conf)

		err := json.Unmarshal(conf, &form)
		if err != nil {
			logurus.Error(err.Error())
			return err
		}
		return nil
	})
	if err != nil {
		logurus.Error(err.Error())
		return "", err
	}

	return form, nil
}

func SetLastOrderID(db *bolt.DB, id int) error {
	confBytes, err := json.Marshal(id)
	if err != nil {
		return fmt.Errorf("could not marshal config json: %v", err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		err = tx.Bucket([]byte("DB")).Put([]byte("LASTORDERID"), confBytes)
		if err != nil {
			return fmt.Errorf("could not set config: %v", err)
		}
		return nil
	})
	return err
}

func GetLastOrderID(db *bolt.DB, id int) (int, error) {
	err := db.View(func(tx *bolt.Tx) error {
		conf := tx.Bucket([]byte("DB")).Get([]byte("LASTORDERID"))
		// fmt.Printf("Config: %s\n", conf)

		err := json.Unmarshal(conf, &id)
		if err != nil {
			logurus.Error(err.Error())
			return err
		}
		return nil
	})
	if err != nil {
		logurus.Error(err.Error())
		return -1, err
	}

	return id, nil
}
