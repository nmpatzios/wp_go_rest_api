package printers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	logurus "github.com/sirupsen/logrus"
)

var templateFuncMap = template.FuncMap{
	"arrays": arrays,
	"trim":   trim,
}
var debugCounter uint64

func arrays(a string) []string {

	return strings.Split(a, ",")
}
func trim(a string) string {
	if strings.ContainsAny(a, "&") {
		return strings.TrimSpace("")
	}

	if strings.TrimSpace(a) != "" {
		return fmt.Sprintf("- %s", strings.TrimSpace(a))
	}
	return strings.TrimSpace("")
}
func nextDebugId() string {

	return fmt.Sprintf("%d", atomic.AddUint64(&debugCounter, 1))
}

func buildFileName() string {
	return time.Now().Format("20060102150405") + "_" + nextDebugId() + ".txt"
}
func sum(input ...int) int {
	sum := 0

	for i := range input {
		sum += input[i]
	}

	return sum
}
func SplitSubN(s string, n int) []string {
	sub := ""
	subs := []string{}

	runes := bytes.Runes([]byte(s))
	l := len(runes)
	for i, r := range runes {
		sub = sub + string(r)
		if (i+1)%n == 0 {
			subs = append(subs, sub)
			sub = ""
		} else if (i + 1) == l {
			subs = append(subs, sub)
		}
	}

	return subs
}

func SendToPrinter(data []byte, limit int, defaultForm string) error {
	// d := net.Dialer{Timeout: 10 * time.Second}
	// conn, err := d.Dial("tcp", "10.10.242.80:9100")
	// if err != nil {
	// 	logurus.Error(err.Error())
	// 	return
	// }
	// println(limit)

	var inInterface []Order
	json.Unmarshal(data, &inInterface)

	wg := &sync.WaitGroup{}

	for _, val := range inInterface {
		wg.Add(1)
		go func(val Order) {
			ti, err := time.Parse("2006-01-02T15:04:05", val.DateCreated)

			if err != nil {
				logurus.Error(err.Error())

			}
			subs := SplitSubN(val.CustomerNote, limit)
			val.CustomerNote = strings.Join(subs[:], "\n")

			val.DateCreated = ti.Format("02/01/2006 15:04")

			for i, j := range val.LineItems {

				// for l := range j.MetaData {
				// 	if j.MetaData[l].Key == "_free_gift" {
				// 		j.MetaData[l].Key = ""
				// 		j.MetaData[l].Value = ""
				// 	}
				// }

				// for k := range val.LineItems[i].MetaData {
				// 	if val.LineItems[i].MetaData[k].Key == "_free_gift" || val.LineItems[i].MetaData[k].Key == "_rule_id_free_gift" {
				// 		val.LineItems[i].MetaData[k] = LineItemMetaData{}

				// 	}
				// }

				name := strings.Split(j.Name, "")
				quantity := strings.Split(strconv.Itoa(j.Quantity), "")
				total := strings.Split(j.Total, "")
				euro := strings.Split("€", "")

				sum := sum(len(name), len(quantity), len(total), len(euro))

				if sum < limit {
					diff := limit - sum
					j.Name = fmt.Sprintf(" %s", j.Name)
					j.Total = fmt.Sprintf("%*s", diff-1, j.Total)
					val.LineItems[i].Product = fmt.Sprintf("%v%s%*s", j.Quantity, j.Name, diff-1, j.Total)
				} else {

					diff := limit - sum
					j.Name = fmt.Sprintf(" %s", strings.Split(j.Name, "-")[0])
					j.Total = fmt.Sprintf("%*s", diff-1, j.Total)
					val.LineItems[i].Product = fmt.Sprintf("%v%s%*s", j.Quantity, j.Name, diff-1, j.Total)
				}
			}

			t := template.Must(template.New("").Funcs(templateFuncMap).Parse(string(defaultForm)))
			builder := &strings.Builder{}
			if err := t.Execute(builder, val); err != nil {
				logurus.Error(err.Error())

			}
			s := builder.String()

			f, err := os.Create("orders/" + buildFileName())
			// f, err := os.Create("/media/sf_orders/" + buildFileName())
			if err != nil {
				logurus.Error(err.Error())

			}

			regex, err := regexp.Compile("\n\n")
			if err != nil {
				logurus.Error(err.Error())

			}
			s = regex.ReplaceAllString(s, "\n")

			_, err = f.WriteString(s)
			if err != nil {
				logurus.Error(err.Error())
				f.Close()

			}
			// fmt.Println(l, "bytes written successfully")
			// logurus.Infof("file created successfully  filename: %s", f.Name())
			err = f.Close()
			if err != nil {
				logurus.Error(err.Error())

			}

			// all := strings.Split(s, "")

			// var words []string

			// for _, char := range all {
			// 	words = append(words, MapKeys(char))
			// 	// fmt.Printf("character %s\n", MapKeys(strconv.QuoteRune(char)))

			// 	// fmt.Println(strconv.QuoteRuneToASCII(char))
			// }
			// // fmt.Println(strings.Join(words, ""))

			// w := bufio.NewWriter(conn)
			// f, err := os.OpenFile("receipt.txt", os.O_RDWR, 0)
			// if err != nil {
			// 	logurus.Error(err.Error())
			// }
			// defer f.Close()
			// p := escpos.New(f)

			// p.Verbose = true

			// p.Init()
			// // p.SetFontSize(2, 3)
			// // p.SetFont("A")
			// // p.Write(strings.Join(words, ""))
			// //p.SetUnderline(1)
			// p.SetFontSize(1, 2)
			// p.Write(s)

			// // p.SetReverse(1)
			// // p.SetFontSize(2, 2)
			// // p.Write("4PITA KALAMAKI")
			// p.FormfeedN(10)
			// p.FormfeedD(200)

			// p.Cut()

			// w.Flush()

			wg.Done()
		}(val)

	}
	wg.Wait()

	return nil
}
