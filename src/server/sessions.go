package main

import (
	"net/http"
	"time"

	"github.com/gorilla/securecookie"
)

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

func getUserName(r *http.Request) (userName string) {
	if cookie, err := r.Cookie("wordpress"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("wordpress", cookie.Value, &cookieValue); err == nil {
			userName = cookieValue["name"]
		}
	}
	return userName
}

func setSession(UserName string, w http.ResponseWriter) {
	value := map[string]string{
		"name": UserName,
	}
	if encoded, err := cookieHandler.Encode("wordpress", value); err == nil {
		cookie := &http.Cookie{
			Name:   "wordpress",
			Value:  encoded,
			Path:   "/",
			MaxAge: 31556926,
		}
		http.SetCookie(w, cookie)
	}
}

func clearSession(w http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "wordpress",
		Value:  "",
		Path:   "/",
		MaxAge: int(31556926 * time.Second),
	}
	http.SetCookie(w, cookie)
}
