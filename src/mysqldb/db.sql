CREATE DATABASE salon_new;
USE salon_new;

CREATE TABLE IF NOT EXISTS users(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    role VARCHAR(120),
    forename VARCHAR(120),
    surname VARCHAR(120),
    work_telephone VARCHAR(120) NULL,
    mobile_telephone VARCHAR(120) NULL, 
    company_name VARCHAR(200) NOT NULL,
    username VARCHAR (50),
    email VARCHAR(50) NOT NULL ,
    password VARCHAR (120),
    is_active BOOL,
    logo_image VARCHAR(60),
    created_at DATETIME NULL,
    updated_at DATETIME NULL,
    last_login DATETIME,
    UNIQUE KEY username (username)
    )CHARSET = utf8;