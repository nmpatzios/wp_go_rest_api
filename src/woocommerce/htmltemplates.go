package woocommerce

import (
	"bytes"
	"html/template"
	"io"
	"time"
)

var templateFuncMap = template.FuncMap{
	"safehtml": safeHTML,
	"add":      add,
	"date":     date,
}

func date(date string) string {
	yourDate, _ := time.Parse("2006-01-02T15:04:05", date)
	return yourDate.Format("02-1-2006 15:04")
}

func safeHTML(html string) template.HTML {
	return template.HTML(html)
}
func add(x, y int) int {
	return x + y
}

var OrdersTemplateHTML = template.Must(template.New("").Funcs(templateFuncMap).Parse(`



{{ $length := len . }} 

<span class="hidden lenorders">{{$length}}</span>


		 
{{range .}}

<div class="col-xs-12 profile_details contact-name">

<div class="well profile_view">
    <div class="col-sm-12">
        <h3>Παραγγελία #{{.ID}}  <span class="">{{date .DateCreated}}</span></h3>
        {{$total :=.Total}} {{$shippingtotal :=.ShippingTotal}}
        <h4 class="brief"><i>Παράδοση/Πληρωμή:{{.ShippingMethod}}/{{.PaymentMethodTitle}}. <br/>
        Σχόλια Παραγγελίας: {{.CustomerNote}}</i></h4>
        <div class="left col-md-3 col-xs-6">
            <h3>Χρέωση</h3>
            <p><strong>'Ονομα: </strong> {{.Billing.FirstName}} {{.Billing.LastName}} </p>
            <ul class="list-unstyled">
                <li><i class="fa fa-building"></i> Εταιρία: {{.Billing.Company}} </li>
                <li><i class="fa fa-home"></i> Διεύθυνση 1 : {{.Billing.Address1}}</li>
                <li><i class="fa fa-building-o"></i> Πόλη : {{.Billing.City}}</li>
                <li><i class="fa fa-info"></i> Τ.Κ. : {{.Billing.Postcode }}</li>
                <li><i class="fa fa-phone"></i> Τηλέφωνο : {{.Billing.Phone}} </li>
                <li><i class="fa fa-envelope"></i> E-mail : {{.Billing.Email}}</li>
            </ul>
        </div>

        <div id="" class=" right col-md-9 col-xs-12 ">
            <h3>Προϊόντα</h3>

            <ul class="list-unstyled top_profiles scroll-view">
                {{range .LineItems}}
                <li class="media event">
                    <a class="pull-left border-aero profile_thumb">
                        <i class="fa fa-shopping-cart aero"></i>
                    </a>
                    <div class="media-body">
                        <a class="title" href="#">{{.Name}}</a>
                        <p class="totalright">
                            <strong>€{{printf "%.2f" .Price}} x {{.Quantity}}</strong>
                            Σύνολο:€{{.Total}}
                        </p>
                        <p> Σχόλια είδους:<br />
                            {{range .MetaData}}
                            <small><strong>{{safehtml .Value}}</strong></small>

                            {{end}}
                        </p>
                    </div>
                </li>
                {{end}}


            </ul>
            <table class="table">

                <tbody>
                <tr>
                        <td>Έκπτωση Κουπόνι</td>
                        <td class="totalright">€{{.DiscountTotal}}</td>
                    </tr>

                    <tr>
                        <td>Σύνολο</td>
                        <td class="totalright">€{{$total}}</td>
                    </tr>


                </tbody>
            </table>
        </div>
    </div>
    <div class="col-xs-12 bottom">
        <div class="col-xs-12 col-sm-6 emphasis">

            <button type="button" status="completed" class="btn btn-success statusbutton"
                onclick='status("completed",{{.ID}});'>
                <i class="fa fa-thumbs-o-up"> </i> Ολοκληρώθηκε
            </button>
            <button type="button" status="cancelled" class="btn btn-danger statusbutton"
                onclick=' status("cancelled",{{.ID}});'>
                <i class="fa fa-times"> </i> Ακυρώθηκε
            </button>
           
        </div>
       
        <div class="col-xs-12 col-sm-6 emphasis">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"  data-lat='{{.Lat}}' data-lng='{{.Lon}}' data-orderid='{{.ID}}' data-billingname='{{.Billing.FirstName}} {{.Billing.LastName}}' data-billingphone='{{.Billing.Phone}}' data-billingaddress='{{.Billing.Address1}}'>
        Χάρτης
      </button>
            <button type="button" tiposi="{{.ID}}" class="btn btn-warning float-right"   onclick='tiposi({{.ID}},{{.}});'>
                <i class="fa fa-print"> </i> Επανεκτύπωση
            </button>
            <button type="button" orderid="{{.ID}}" class="btn btn-info float-right customerupdate" data-toggle="modal" data-target=".bs-example-modal-md"  onclick='orderid({{.ID}});'>
              <i class="fa fa-sign-in"> </i> Ενημέρωση Πελάτη
            </button>
        </div>


    </div>
</div>
</div>
{{end}}
	
`))

type ttemplate interface {
	Execute(io.Writer, interface{}) error
}

func RenderOrderTemplateString(tpl ttemplate, data interface{}) (string, error) {
	var buf bytes.Buffer
	if err := tpl.Execute(&buf, data); err != nil {
		println("TEMPLATE ERROR", err.Error())
		return "", err
	}
	return buf.String(), nil
}
