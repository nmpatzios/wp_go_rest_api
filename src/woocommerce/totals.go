package woocommerce

import (
	"encoding/json"
	"io"
	"net/url"
	"time"

	logurus "github.com/sirupsen/logrus"
)

func (c *Client) GeTotals() []Total {

	var data []Total

	body, err := c.Get("reports/orders/totals", nil)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error  getting precessing orders")
	}

	jsonDecoder := json.NewDecoder(body)
	if err := jsonDecoder.Decode(&data); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"jsonDecoder": jsonDecoder,
			"error":       err.Error(),
		}).Errorln("Error decoding  json")
	}
	defer body.Close()

	return data

}

func (c *Client) GetTotalAMounts(activeAt string) []TotalAMounts {
	var amounts []TotalAMounts
	v := url.Values{}
	t := time.Now()
	v.Add("date_min", activeAt)
	v.Add("date_max", t.Format("2006-01-02"))

	body, err := c.Get("reports/sales", v)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error getting totals ")
	}

	jsonDecoder := json.NewDecoder(body)
	if err := jsonDecoder.Decode(&amounts); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"jsonDecoder": jsonDecoder,
			"error":       err.Error(),
		}).Errorln("Error decoding  json")
	}
	defer body.Close()

	return amounts

}

func (c *Client) GetTotalAMountsCustomRange(from, to string) []TotalAMounts {
	var amounts []TotalAMounts
	v := url.Values{}

	v.Add("date_min", from)
	v.Add("date_max", to)

	body, err := c.Get("reports/sales", v)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error getting custom totals")
	}

	jsonDecoder := json.NewDecoder(body)
	if err := jsonDecoder.Decode(&amounts); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"jsonDecoder": jsonDecoder,
			"error":       err.Error(),
		}).Errorln("Error decoding  json")
	}
	defer body.Close()

	return amounts

}

func (c *Client) GetTotalAMountsPerDay() []TotalAMounts {
	var amounts []TotalAMounts
	v := url.Values{}
	t := time.Now()
	v.Add("date", t.Format("2006-01-02"))

	body, err := c.Get("reports/sales", v)
	if err != nil {
		logurus.WithFields(logurus.Fields{
			"body":  body,
			"error": err.Error(),
		}).Errorln("Error getting completed  orders")
	}

	jsonDecoder := json.NewDecoder(body)
	if err := jsonDecoder.Decode(&amounts); err != nil && err != io.EOF {
		logurus.WithFields(logurus.Fields{
			"jsonDecoder": jsonDecoder,
			"error":       err.Error(),
		}).Errorln("Error decoding  json")
	}
	defer body.Close()

	return amounts

}
