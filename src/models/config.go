package models

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"

	logurus "github.com/sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

var (
	KeyGenerator = []byte("NcRfUjXn2r5u8x/A")
)

type Conf struct {
	MYSQLUSER        string `yaml:"MYSQL_USER"`
	MYSQLPASS        string `yaml:"MYSQL_PASS"`
	MYSQLHOST        string `yaml:"MYSQL_HOST"`
	MYSQLDB          string `yaml:"MYSQL_DB"`
	MYSQLTLS         string `yaml:"MYSQL_TLS"`
	HTTPPLATFORMPORT string `yaml:"HTTP_PLATFORM_PORT"`
	ADMINPASS        string `yaml:"ADMIN_PASS"`
	TIMER            int    `yaml:"TIMER"`
	EDESIADOMAIN     string `yaml:"EDESIA_DOMAIN"`
	EDESIAKEY        string `yaml:"EDESIA_KEY"`
	EDESIASECRET     string `yaml:"EDESIA_SECRET"`
}

func (c *Conf) GetConf() *Conf {

	yamlFile, err := ioutil.ReadFile("config.yaml")
	if err != nil {

		logurus.Errorf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		logurus.Errorf("Unmarshal: %v", err)
	}

	return c
}
func EncryptPass(key []byte, text string) string {
	// key := []byte(keyText)
	plaintext := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		logurus.Error(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		logurus.Error(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// convert to base64
	return base64.URLEncoding.EncodeToString(ciphertext)
}

// decrypt from base64 to decrypted string
func DecryptPass(key []byte, cryptoText string) string {
	ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher(key)
	if err != nil {
		logurus.Error(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		logurus.Error("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext)
}
